#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "../shared/stringtools.cpp"

enum InstructionType
{
  Spin,
  Exchange,
  Partner,
};

class Instruction
{
  public:
    InstructionType type;
    int num1;
    int num2;
    std::string prog1;
    std::string prog2;

    Instruction(std::string in)
    {

      // Initialization
      num1 = -1;
      num2 = -1;
      prog1 = " ";
      prog2 = " ";

      if (in[0] == 's') type = Spin;
      if (in[0] == 'x') type = Exchange;
      if (in[0] == 'p') type = Partner;

      auto parts = split(in.substr(1,in.size()),'/');

      switch (type)
      {
        case Spin:
          num1 = std::stoi(in.substr(1,in.size()));
          break;

        case Exchange:
          num1 = std::stoi(parts[0]);
          num2 = std::stoi(parts[1]);
          break;

        case Partner:
          prog1 = parts[0];
          prog2 = parts[1];
          break;

        default:
          std::cout << "ERROR: Unknown instruction type.\n" << std::endl;
          exit(EXIT_FAILURE);
          break;
      }
    }
};

void Day16(std::string file)
{
  std::ifstream in(file);

  std::vector<std::string> instrStrings;

  std::vector<Instruction> instructions;

  std::vector<std::string> progs;

  std::vector<std::string> seen;

  const int NUM_PROGS = 16;
  progs.reserve(NUM_PROGS);

  const int NUM_ITER = 1000000000;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      instrStrings = split(line,',');
    }
  }

  // Generate vector of Instruction objects
  instructions.reserve(instrStrings.size());
  for (auto & instrString : instrStrings)
  {
    instructions.push_back(Instruction(instrString));
  }

  // Generate programs alphabet
  for (auto i = 0; i < NUM_PROGS; i++)
  {
    char a = i+97;
    progs.push_back(std::string(1,a));
  }

  // Perform loop
  std::string swap;
  int cycle;
  bool cont = true;
  for (cycle = 0; cycle < NUM_ITER; cycle++)
  {
    for (auto & instr : instructions)
    {
      switch (instr.type)
      {
        case Spin:
          std::rotate(progs.begin(), progs.end() - instr.num1, progs.end());
          break;

        case Exchange:
          swap = progs[instr.num1];
          progs[instr.num1] = progs[instr.num2];
          progs[instr.num2] = swap;
          break;

        case Partner:
          {
          auto res1 = std::find(progs.begin(), progs.end(), instr.prog1);
          auto res2 = std::find(progs.begin(), progs.end(), instr.prog2);

          int ix1 = std::distance(progs.begin(), res1);
          int ix2 = std::distance(progs.begin(), res2);

          swap = progs[ix1];
          progs[ix1] = progs[ix2];
          progs[ix2] = swap;
          }
          break;

        default:
          std::cout << "ERROR: Unknown instruction type.\n" << std::endl;
          exit(EXIT_FAILURE);
          break;
      };
    }

    // Compare against previously seen
    std::string thisConfig;
    for (auto & p : progs) { thisConfig += p; }
    for (auto & prev : seen)
    {
      if (thisConfig == prev) cont = false;
    }
    if (!cont) break;
    seen.push_back(thisConfig);
  }

  // Part 1
  std::cout << "Part 1: " << seen[0] << std::endl; // bkgcdefiholnpmja
  
  // Part 2
  auto ix = NUM_ITER % cycle - 1;
  std::cout << "Part 2: " << seen[ix] << std::endl; // knmdfoijcbpghlea

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day16(argv[1]);

  return 0;
}