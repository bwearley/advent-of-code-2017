#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>

#include "../shared/stringtools.cpp"

struct Day9Result
{
  int groups;
  int score;
  int lastPos;
  int garbage;
};

Day9Result parseGroups(std::string str, int score_in)
{
  Day9Result res;
  res.groups=0;
  res.score=0;
  res.lastPos=0;
  res.garbage=0;

  int thisScore = score_in + 1;
  res.score = thisScore;

  bool readingGarbage = false;

  // Loop through string
  for (int i = 0; i < str.size(); i++)
  {
    res.lastPos = i;
    // Cancel next character within garbage
    if (str[i] == '!' && readingGarbage)
    {
      i += 1; // skip next character
    }
    // Begin reading garbage
    else if (str[i] == '<' && !readingGarbage)
    {
      readingGarbage = true;
    }
    // End reading garbage
    else if (str[i] == '>' && readingGarbage)
    {
      readingGarbage = false;
    }
    // Found new group, recurse
    else if (str[i] == '{' && !readingGarbage)
    {
      res.groups += 1;
      Day9Result subRes = parseGroups(str.substr(i+1), thisScore);
      res.groups += subRes.groups;
      res.score += subRes.score;
      res.garbage += subRes.garbage;
      i += 1 + subRes.lastPos;
    }
    // Finish reading group, return
    else if (str[i] == '}' && !readingGarbage)
    {
      return res;
    }
    // Count non-cancelled characters within garbage
    else if (readingGarbage)
    {
      res.garbage += 1;
    }
  }
  return res;
}

void Day9(std::string file)
{
  std::ifstream in(file);

  std::string inputStream;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    inputStream = line;
  }

  // Lambda with capture by reference (&)
  int score = -1;
  Day9Result result = parseGroups(inputStream, score);
  printf("Groups: %d\n", result.groups);

  printf("Part 1: %d\n", result.score); // 12803
  printf("Part 2: %d\n", result.garbage); // 6425

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day9(argv[1]);

  return 0;
}