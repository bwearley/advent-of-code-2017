#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <regex>
#include <queue>
#include <iomanip>

#include "../shared/stringtools.cpp"

enum ValueType
{
  IntegerValue,
  RegisterValue,
};

class Instruction
{
  public:
    ValueType xValueType;
    ValueType yValueType;
    std::string type;
    std::string Xstr;
    std::string Ystr;
    int Xint;
    int Yint;
    std::string input;

    Instruction(std::string in)
    {

      input = in;

      xValueType = RegisterValue;
      yValueType = RegisterValue;

      auto parts = split(in,' ');

      // Instruction Type
      type = parts[0];

      // Get X and Y
      Xstr = parts[1];

      if (type != "snd" && type != "rcv")
      {
        Ystr = parts[2];
      }
      else
      {
        Ystr = " ";
      }

      // Get integer values if appropriate
      std::regex numRegex("([-]*[0-9]+)");
      if (std::regex_search(Xstr, numRegex)) { xValueType = IntegerValue; Xint = std::stoi(Xstr); }
      if (std::regex_search(Ystr, numRegex)) { yValueType = IntegerValue; Yint = std::stoi(Ystr); }

    }
};

void Day18Part1(std::string file)
{
  std::ifstream in(file);

  std::vector<Instruction> instr;

  const int NUM_REG = 16;

  long reg[NUM_REG] = { 0 };

  int lastFreq = 0;

  int current = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      instr.push_back(Instruction(line));
    }
  }

  auto snd = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = reg[s2i(in.Xstr)]; }
    else { x = in.Xint; }
    lastFreq = x;
  };

  auto set = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] = y;
  };

  auto add = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] += y;
  };

  auto mul = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] *= y;
  };

  auto mod = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] = reg[x] % y;
  };

  auto rcv = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = reg[s2i(in.Xstr)]; }
    else { x = in.Xint; }
    if (x == 0) return;
  };

  auto jgz = [&](Instruction &in)
  {
    int x;
    if (in.xValueType == RegisterValue) { x = reg[s2i(in.Xstr)]; }
    else { x = in.Xint; }
    int y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    if (x > 0) current += y-1;
    //if (x <= 0) current += 1;
  };

  // Main loop
  for ( ; ; )
  {
    Instruction* cur = &instr[current];

    if (cur->type == "snd") { snd(*cur); }
    if (cur->type == "set") { set(*cur); }
    if (cur->type == "add") { add(*cur); }
    if (cur->type == "mul") { mul(*cur); }
    if (cur->type == "mod") { mod(*cur); }
    if (cur->type == "rcv") { rcv(*cur); break; }
    if (cur->type == "jgz") { jgz(*cur); }

    // Debug
    /*
    std::cout << "Registers:\n";
    for (int k = 0; k < NUM_REG; k++)
    {
      std::cout <<" " << i2s(k);
    }
    std::cout << std::endl;
    for (int k = 0; k < NUM_REG; k++)
    {
      std::cout << " " << reg[k];
    }
    std::cout << std::endl;
    */

    current += 1;

    if (current >= instr.size()) break;
  }

  printf("Part 1: %d\n",lastFreq); // 2951

}

void Day18Part2(std::string file)
{
  std::ifstream in(file);

  std::vector<Instruction> instr;

  const int NUM_PROG = 2;

  const int NUM_REG = 16;

  int64_t reg[NUM_PROG][NUM_REG] = { 0 };

  int current[NUM_PROG] = { 0 };

  std::queue<int64_t> q[2];

  bool lock[NUM_PROG] = { false, false };

  bool running[NUM_PROG] = { true, true };

  int sentValues[NUM_PROG] = { 0 };

  //std::vector<int64_t> sentValuesLog[2];

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      instr.push_back(Instruction(line));
    }
  }

  auto snd = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = reg[prog][s2i(in.Xstr)]; }
    else { x = in.Xint; }
    q[prog].push(x);
    //std::cout << "-->Program " << prog << " sending value: " << x << std::endl;
    sentValues[prog] += 1;
    //sentValuesLog[prog].push_back(x);
  };

  auto rcv = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }

    int fromProg = (prog == 1 ? 0 : 1);
    if (!q[fromProg].empty())
    {
      lock[prog] = false;
      reg[prog][x] = q[fromProg].front();
      q[fromProg].pop();
      //std::cout << "<--Program " << prog << " receiving value: " << reg[prog][x] << std::endl;
    }
    else
    {
      lock[prog] = true;
      //current[prog] -= 1; // pause here
      //std::cout << "Program " << prog << " paused awaiting snd from " << fromProg << std::endl;
    }
  };

  auto set = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[prog][s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[prog][x] = y;
  };

  auto add = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[prog][s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[prog][x] += y;
  };

  auto mul = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[prog][s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[prog][x] *= y;
  };

  auto mod = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[prog][s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[prog][x] %= y;
  };

  auto jgz = [&](int prog, Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = reg[prog][s2i(in.Xstr)]; }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[prog][s2i(in.Ystr)]; }
    else { y = in.Yint; }
    if (x > 0) current[prog] += y-1; //-1 because current[prog] will still get incremented by 1
  };

  // Initialize
  for (int i = 0; i < NUM_PROG; i++)
  {
    reg[i][s2i("p")] = i;
  }

  // Main loop
  int64_t cycles = 0;
  for ( ; ; )
  {
    cycles += 1;
    
    // Debug
    /*std::cout << "Registers: (" << cycles << ")" << std::endl;
    for (int k = 0; k < NUM_REG; k++)
    {
      std::cout << std::setw(10) << i2s(k);
    }
    std::cout << std::endl;*/

    for (int i = 0; i < NUM_PROG; i++)
    {

      if (!running[i]) continue;

      //std::cout << "<" << i << ">" << instr[current[i]].input << " ("  << current[i] << ")" << std::endl;

      if (instr[current[i]].type == "snd") { snd(i, instr[current[i]]); }
      if (instr[current[i]].type == "set") { set(i, instr[current[i]]); }
      if (instr[current[i]].type == "add") { add(i, instr[current[i]]); }
      if (instr[current[i]].type == "mul") { mul(i, instr[current[i]]); }
      if (instr[current[i]].type == "mod") { mod(i, instr[current[i]]); }
      if (instr[current[i]].type == "rcv") { rcv(i, instr[current[i]]); }
      if (instr[current[i]].type == "jgz") { jgz(i, instr[current[i]]); }

      if (!lock[i]) current[i] += 1;
    }

    // Debug
    /*for (int i = 0; i < NUM_PROG; i++)
    {
      for (int k = 0; k < NUM_REG; k++)
      {
        std::cout << std::setw(10) << reg[i][k];
      }
      std::cout << std::endl;
    }*/

    // Determine if programs finished naturally
    for (int i = 0; i < NUM_PROG; i++) { if (current[i] >= instr.size()) running[i] = false; }

    // Terminate if both programs deadlocked or both programs finished
    if ((lock[0] && lock[1]) || (!running[0] && !running[1])) break;

  }

  // Debug
  //if (lock[0] && lock[1]) printf("Programs ended due to deadlock.\n");
  //if (!running[0] && !running[1]) printf("Programs ended naturally.\n");

  printf("Part 2: %d\n",sentValues[1]); // 7366

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day18Part1(argv[1]);
  Day18Part2(argv[1]);

  return 0;
}