#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../shared/stringtools.cpp"

#include "../shared/knothash.cpp"

#include "../shared/map.cpp"

const char* hex_char_to_bin(char c)
{
  switch (std::toupper(c))
  {
    case '0' : return "0000";
    case '1' : return "0001";
    case '2' : return "0010";
    case '3' : return "0011";
    case '4' : return "0100";
    case '5' : return "0101";
    case '6' : return "0110";
    case '7' : return "0111";
    case '8' : return "1000";
    case '9' : return "1001";
    case 'A' : return "1010";
    case 'B' : return "1011";
    case 'C' : return "1100";
    case 'D' : return "1101";
    case 'E' : return "1110";
    case 'F' : return "1111";
  }
}

std::string hex_to_bin(const std::string& hex)
{
  std::string bin;
  for (unsigned i = 0; i != hex.length(); ++i)
  {
    bin += hex_char_to_bin(hex[i]);
  }
  return bin;
}

std::vector<Point*> visit(Map & map, Point & p)
{
  // Process self
  std::vector<Point*> pts;
  p.visited = true;
  pts.push_back(&p);
  
  // Process neighbors
  auto neighbors = map.getNSEWNeighbors(&p);
  for (auto & n : neighbors)
  {
    if (n->visited || n->value != 1) continue;
    pts.push_back(n);
    auto others = visit(map, *n);
    for (auto & n0 : others)
    {
      pts.push_back(n0);
    }
  }

  return pts;
}

void Day14(std::string file)
{
  std::ifstream in(file);

  std::string input;

  const int NUM_ROWS = 128;

  std::string rows[NUM_ROWS];
  std::string hashes[NUM_ROWS];
  std::string bits[NUM_ROWS];

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      input = line;
    }
  }

  // Generate hashes and bits
  for (int i = 0; i < NUM_ROWS; i++)
  {
    rows[i] = input + '-' + std::to_string(i);
    hashes[i] = knotHash(rows[i]);
    bits[i] = hex_to_bin(hashes[i]);
  }
  
  // Part 1 -- count used
  int part1 = 0;
  for (int i = 0; i < NUM_ROWS; i++)
  {
    for (unsigned j = 0; j != bits[i].length(); ++j)
    {
      if (bits[i][j] == '1') part1 += 1;
    }
  }
  printf("Part 1: %d\n", part1); // 8222

  // Part 2 -- generate map
  Map map(0,NUM_ROWS,0,NUM_ROWS);
  for (int i = 0; i < NUM_ROWS; i++)
  {
    for (unsigned j = 0; j != bits[i].length(); ++j)
    {
      if (bits[i][j] == '1') map.point(j,i)->setValue(1);
    }
  }

  // Part 2 -- generate regions
  std::vector<std::vector<Point*>> regions;
  for (int y = 0; y < NUM_ROWS; y++)
  {
    for (int x = 0; x < NUM_ROWS; x++)
    {
      Point* thisPt = map.point(x,y);

      if (thisPt->value != 1 || thisPt->visited) continue;

      regions.push_back(visit(map, *thisPt));
    }
  }

  // Part 2 -- output
  printf("Part 2: %d\n", regions.size()); // 1086
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day14(argv[1]);

  return 0;
}