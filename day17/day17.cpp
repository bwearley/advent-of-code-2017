#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../shared/stringtools.cpp"

class Node
{
  public:
    Node* left;
    Node* right;
    int value;

    Node()
    {
      value = 0;
      left = NULL;
      right = NULL;
    }

    Node(int x0)
    {
      value = x0;
      left = NULL;
      right = NULL;
    }
};

void Day17(std::string file)
{
  std::ifstream in(file);

  int input = 0;

  const int NUM_CYCLES_PART_1 = 2017;
  const int NUM_CYCLES_PART_2 = 50000000;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      input = std::stoi(line);;
    }
  }

  //printf("Input: %d\n", input);

  // Initialize circle
  std::vector<Node> nodes(NUM_CYCLES_PART_2+1);
  Node* current = &nodes[0];
  current->left = &nodes[0];
  current->right = &nodes[0];

  // Main loop
  for (int i = 1; i <= NUM_CYCLES_PART_2; i++)
  {

    // Step forward
    for (int m = 1; m <= input; m++)
    {
      current = current->right;
    }

    // Insert new node
    nodes[i] = Node(i);
    Node* newNode = &nodes[i];

    // New node links
    newNode->left = current;
    Node* currentRight = current->right;
    newNode->right = currentRight;

    // Break and recreate links
    currentRight->left = newNode;
    current->right = newNode;

    // Move current to new node
    current = newNode;

    // Part 1 Output
    if (i == NUM_CYCLES_PART_1) printf("Part 1: %d\n", current->right->value); // 1173

  }

  // Part 2 Output
  printf("Part 2: %d\n", nodes[0].right->value); // 1930815

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day17(argv[1]);

  return 0;
}