#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>

#include "../shared/stringtools.cpp"

int indexOfFirstMaxElement(std::vector<int> vec)
{
  return std::distance(vec.begin(), std::max_element(vec.begin(), vec.end()));
}

void Day6(std::string file)
{
  std::ifstream in(file);

  std::vector<int> banks;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      // Get values as strings
      auto values = split(line, '\t'); // tab-delimited file
      
      // Convert strings to integers
      for (auto i = 0; i < values.size(); ++i)
      {
        banks.push_back(std::stoi(values[i]));
      }     
    }
  }

  int numBanks = banks.size();

  std::vector<std::vector<int>> seenConfigs;

  int cycle = 0;
  for ( ; ; )
  {
    cycle += 1;
    int maxBank = indexOfFirstMaxElement(banks);

    // Save and empty current bank
    int blocksToDistrib = banks[maxBank];
    banks[maxBank] = 0;
    //printf("Cycle %d : MaxBank %d (val: %d)\n", cycle, maxBank, banks[maxBank]);
    int j = 0;
    for (auto i = blocksToDistrib; i > 0; i--)
    {
      j += 1;
      int thisBank = maxBank + j;
      if (thisBank == numBanks) { thisBank -= numBanks; j -= numBanks; } // wrap back around
      //printf("Distributing 1 block to bank %d; %d blocks remaining.\n", thisBank, i-1);
      banks[thisBank] += 1;    
    }

    bool end = false;
    for (auto & seenConfig : seenConfigs)
    {
      if (banks == seenConfig)
      {
        end = true;
        break;
      }
    }
    if (end) break;

    seenConfigs.push_back(banks);

  }
  printf("Part 1: %d\n", cycle); // 5042

  // Part 2 - redundant test loop
  int part2 = 0;
  for (auto i = 0; i < seenConfigs.size(); i++)
  {
    if (seenConfigs[i] == banks)
    {
      part2 = i;
      break;
    }
  }
  printf("Part 2: %d\n", seenConfigs.size() - part2); // 1086

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day6(argv[1]);

  return 0;
}