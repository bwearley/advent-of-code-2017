#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <regex>
#include <queue>
#include <iomanip>
#include <math.h>

#include "../shared/stringtools.cpp"

enum ValueType
{
  IntegerValue,
  RegisterValue,
};

class Instruction
{
  public:
    ValueType xValueType;
    ValueType yValueType;
    std::string type;
    std::string Xstr;
    std::string Ystr;
    int Xint;
    int Yint;
    std::string input;
    int invoked;

    Instruction(std::string in)
    {

      input = in;
      invoked = 0;

      xValueType = RegisterValue;
      yValueType = RegisterValue;

      auto parts = split(in,' ');

      // Instruction Type
      type = parts[0];

      // Get X and Y
      Xstr = parts[1];
      Ystr = parts[2];

      // Get integer values if appropriate
      std::regex numRegex("([-]*[0-9]+)");
      if (std::regex_search(Xstr, numRegex)) { xValueType = IntegerValue; Xint = std::stoi(Xstr); }
      if (std::regex_search(Ystr, numRegex)) { yValueType = IntegerValue; Yint = std::stoi(Ystr); }

    }
};

void Day23Part1(std::string file)
{
  std::ifstream in(file);

  std::vector<Instruction> instr;

  const int NUM_REG = 8; // a ... h

  int64_t reg[NUM_REG] = { 0 };

  int current = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      instr.push_back(Instruction(line));
    }
  }

  auto set = [&](Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] = y;
  };

  auto sub = [&](Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] -= y;
  };

  auto mul = [&](Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    reg[x] *= y;
  };

  auto jnz = [&](Instruction &in)
  {
    int64_t x;
    if (in.xValueType == RegisterValue) { x = reg[s2i(in.Xstr)]; }
    else { x = in.Xint; }
    int64_t y;
    if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
    else { y = in.Yint; }
    if (x != 0) current += y-1; //-1 because current will still get incremented by 1
  };

  // Part 1 Answer
  int64_t mulInvok = 0;

  // Main loop
  int64_t cycles = 0;
  for ( ; ; )
  {
    cycles += 1;
    
    if (instr[current].type == "set") { set(instr[current]); }
    if (instr[current].type == "sub") { sub(instr[current]); }
    if (instr[current].type == "mul") { mul(instr[current]); mulInvok += 1; }
    if (instr[current].type == "jnz") { jnz(instr[current]); }

    current += 1;
    if (current >= instr.size()) break;
  }

  printf("Part 1: %d\n",mulInvok); // 6724

}

void Day23Part2(std::string file)
{
  std::ifstream in(file);

  std::vector<Instruction> instr;

  const int NUM_REG = 8; // a ... h

  int64_t reg[NUM_REG] = { 0 };

  int current = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      instr.push_back(Instruction(line));
    }
  }

  // auto set = [&](Instruction &in)
  // {
  //   int64_t x;
  //   if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
  //   else { x = in.Xint; }
  //   int64_t y;
  //   if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
  //   else { y = in.Yint; }
  //   reg[x] = y;
  // };

  // auto sub = [&](Instruction &in)
  // {
  //   int64_t x;
  //   if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
  //   else { x = in.Xint; }
  //   int64_t y;
  //   if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
  //   else { y = in.Yint; }
  //   reg[x] -= y;
  // };

  // auto mul = [&](Instruction &in)
  // {
  //   int64_t x;
  //   if (in.xValueType == RegisterValue) { x = s2i(in.Xstr); }
  //   else { x = in.Xint; }
  //   int64_t y;
  //   if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
  //   else { y = in.Yint; }
  //   reg[x] *= y;
  // };

  // auto jnz = [&](Instruction &in)
  // {
  //   int64_t x;
  //   if (in.xValueType == RegisterValue) { x = reg[s2i(in.Xstr)]; }
  //   else { x = in.Xint; }
  //   int64_t y;
  //   if (in.yValueType == RegisterValue) { y = reg[s2i(in.Ystr)]; }
  //   else { y = in.Yint; }
  //   if (x != 0) current += y-1; //-1 because current will still get incremented by 1
  // };

  // Initialize
  reg[s2i("a")] = 1;

  auto isPrime = [](int in)
  {
    for (auto i = 2; i <= std::sqrt(in); i++)
    {
      if (in % i == 0) return false;
    }
    if (in == 1) return false;
    return true;
  };

  // Replacemnet Main Loop
  reg[s2i("h")] = 0;
  for (reg[s2i("b")] = 108400; reg[s2i("b")] <= 125400; reg[s2i("b")] += 17)
  {
    if (!isPrime(reg[s2i("b")])) reg[s2i("h")] += 1;

  }

  // Main loop
  // int64_t cycles = 0;
  // for ( ; ; )
  // {
  //   cycles += 1;
    
  //   if (instr[current].type == "set") { set(instr[current]); }
  //   if (instr[current].type == "sub") { sub(instr[current]); }
  //   if (instr[current].type == "mul") { mul(instr[current]); }
  //   if (instr[current].type == "jnz") { jnz(instr[current]); }

  //   current += 1;
  //   if (current >= instr.size()) break;
  // }

  printf("Part 2: %d\n",reg[s2i("h")]); // 903

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day23Part1(argv[1]);
  Day23Part2(argv[1]);

  return 0;
}