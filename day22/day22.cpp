#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <array>

#include <algorithm>

#include "../shared/rotation.cpp"
#include "../shared/map.cpp"

void Day22Part1(std::string file)
{
  std::ifstream in(file);

  const int X = 0;
  const int Y = 1;

  const int CLEAN = 0;
  const int INFECTED = 1;

  const int NUM_BURSTS = 10000;

  // Initialize map with clean values
  const int MIN_DIM = -200;
  const int MAX_DIM = 200;
  Map map = Map(MIN_DIM,MAX_DIM,MIN_DIM,MAX_DIM);
  for (auto y = MIN_DIM; y <= MAX_DIM; y++)
  {
    for (auto x = MIN_DIM; x <= MAX_DIM; x++)
    {
      map.point(x,y)->setValue(CLEAN);
    }
  }

  // Load file data
  int row = 0;
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      for (auto col = 0; col < line.size(); col++)
      {
        if      (line[col] == '#') { map.point(col,row)->setValue(INFECTED); }
        else if (line[col] == '.') { map.point(col,row)->setValue(CLEAN);    }
        else
        {
          std::cout << "ERROR: Unknown map character: " << line[col] << std::endl;
          exit(EXIT_FAILURE);
        }
      }
      row += 1;
    }
  }

  // Initial position...
  std::array<int, 2> pos = { 12, 12 }; // ...in the middle...
  std::array<int, 2> dir = {0, -1}; // ...facing up.

  auto printMap = [&]()
  {
    for (auto y = MIN_DIM; y <= MAX_DIM; y++)
    {
      for (auto x = MIN_DIM; x <= MAX_DIM; x++)
      {
        if (x == pos[X] && y == pos[Y])
        {
          std::cout << "X";
        }
        else if (map.point(x,y)->value == INFECTED)
        {
          std::cout << "#";
        }
        else if (map.point(x,y)->value == CLEAN)
        {
          std::cout << ".";
        }
        else
        {
          std::cout << std::endl;
          printf("ERROR: Unknown map value at (%d,%d)\n", x, y);
          exit(EXIT_FAILURE);
        }
      }
      std::cout << std::endl;
    }
  };

  //printf("Initial map:\n");
  //printMap();

  int part1 = 0;
  for (int burst = 0; burst < NUM_BURSTS; burst++)
  {

    // Turn (NOTE: CW/CCW nomenclature inverted)
    if (map.point(pos[X],pos[Y])->value == INFECTED)
    {
      dir = rotation(Counterclockwise, dir); // Right
    }
    else
    {
      dir = rotation(Clockwise, dir); // Left
    }

    // Infect or clean
    if (map.point(pos[X],pos[Y])->value == CLEAN)
    {
      // Infect
      map.point(pos[X],pos[Y])->setValue(INFECTED);
      part1 += 1;
    }
    else
    {
      // Clean
      map.point(pos[X],pos[Y])->setValue(CLEAN);
    }

    // Move forward 1 node in new direction
    pos = {pos[X] + dir[X],
           pos[Y] + dir[Y]};
  }
  printf("Part 1: %d\n", part1); // 5538
}

void Day22Part2(std::string file)
{
  std::ifstream in(file);

  const int X = 0;
  const int Y = 1;

  const int CLEAN = 0;
  const int INFECTED = 1;
  const int WEAKENED = 2;
  const int FLAGGED = 3;

  const int NUM_BURSTS = 10000000;

  // Initialize map with clean values
  const int MIN_DIM = -300;
  const int MAX_DIM = 300;
  Map map = Map(MIN_DIM,MAX_DIM,MIN_DIM,MAX_DIM);
  for (auto y = MIN_DIM; y <= MAX_DIM; y++)
  {
    for (auto x = MIN_DIM; x <= MAX_DIM; x++)
    {
      map.point(x,y)->setValue(CLEAN);
    }
  }

  // Load file data
  int row = 0;
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      for (auto col = 0; col < line.size(); col++)
      {
        if      (line[col] == '#') { map.point(col,row)->setValue(INFECTED); }
        else if (line[col] == '.') { map.point(col,row)->setValue(CLEAN);    }
        else
        {
          std::cout << "ERROR: Unknown map character: " << line[col] << std::endl;
          exit(EXIT_FAILURE);
        }
      }
      row += 1;
    }
  }

  // Initial position...
  std::array<int, 2> pos = { 12, 12 }; // ...in the middle...
  std::array<int, 2> dir = {0, -1}; // ...facing up.

  auto printMap = [&]()
  {
    for (auto y = MIN_DIM; y <= MAX_DIM; y++)
    {
      for (auto x = MIN_DIM; x <= MAX_DIM; x++)
      {
        if (x == pos[X] && y == pos[Y])
        {
          std::cout << "X";
        }
        else if (map.point(x,y)->value == INFECTED)
        {
          std::cout << "#";
        }
        else if (map.point(x,y)->value == CLEAN)
        {
          std::cout << ".";
        }
        else
        {
          std::cout << std::endl;
          printf("ERROR: Unknown map value at (%d,%d)\n", x, y);
          exit(EXIT_FAILURE);
        }
      }
      std::cout << std::endl;
    }
  };

  //printf("Initial map:\n");
  //printMap();

  int part2 = 0;
  for (int burst = 0; burst < NUM_BURSTS; burst++)
  {

    // Turn (NOTE: CW/CCW nomenclature inverted)
    if (map.point(pos[X],pos[Y])->value == INFECTED)
    {
      dir = rotation(Counterclockwise, dir); // Right
    }
    else if (map.point(pos[X],pos[Y])->value == CLEAN)
    {
      dir = rotation(Clockwise, dir); // Left
    }
    else if (map.point(pos[X],pos[Y])->value == FLAGGED)
    {
      dir = { -dir[X], -dir[Y] };
    }
    else
    {
      // Weakened: do nothing
    }

    // Infect/clean/weaken/flag
    if (map.point(pos[X],pos[Y])->value == CLEAN)
    {
      map.point(pos[X],pos[Y])->setValue(WEAKENED);
    }
    else if (map.point(pos[X],pos[Y])->value == WEAKENED)
    {
      part2 += 1;
      map.point(pos[X],pos[Y])->setValue(INFECTED);
    }
    else if (map.point(pos[X],pos[Y])->value == INFECTED)
    {
      map.point(pos[X],pos[Y])->setValue(FLAGGED);
    }
    else if (map.point(pos[X],pos[Y])->value == FLAGGED)
    {
      map.point(pos[X],pos[Y])->setValue(CLEAN);
    }
    else
    {
      printf("ERROR: Unknown status at (%d,%d)=%d\n",
        pos[X], pos[Y], map.point(pos[X],pos[Y])->value);
      exit(EXIT_FAILURE);
    }   

    // Move forward 1 node in new direction
    pos = {pos[X] + dir[X],
           pos[Y] + dir[Y]};
  }

  printf("Part 2: %d\n", part2); // 2511090
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day22Part1(argv[1]);
  Day22Part2(argv[1]);

  return 0;
}