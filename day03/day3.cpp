#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <array>
#include <iomanip>

#include "../shared/manhattan.cpp"
#include "../shared/map.cpp"

#include "../shared/rotation.cpp"

/*
Col -3  -2  -1   0   1   2   3   4     Row      Length
            <--- X --->
65  64  63  62  61  60  59  58  57
66  37  36  35  34  33  32  31  56           ^
67  38  17  16  15  14  13  30  55     -2    |
68  39  18   5   4   3  12  29  54     -1    |
69  40  19   6   1   2  11  28  53      0    Y       1
70  41  20   7   8  3^2 10  27  52      1    |       3
71  42  21  22  23  24 5^2  26  51      2    |       5
72  43  44  45  46  47  48 7^2  50      3    v       7
73  74  75  76  77  78  79  80 9^2
 */

int sumNeighbors(std::vector<Point*> neighbors)
{
  int sum = 0;
  for (auto & neighbor : neighbors)
  {
    sum += neighbor->value;
  }
  return sum;
}

void Day3Part1(std::string file)
{
  std::ifstream in(file);

  int input = 0;

  struct Point
  {
    int x;
    int y;
  };
  Point part1pt;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      input = std::stoi(line);
    }

    // Get value of the next square value larger than input
    int nextSquare = 0;
    int rowAndColOfSquare = 0;
    for (int i = 1; ; i+=2)
    {
      if (pow(i,2) > input)
      {
        nextSquare = pow(i,2);
        printf("Input: %d\n", input);
        printf("Next square: %d^2=%d\n", i, nextSquare);
        break;
      }
      rowAndColOfSquare += 1;
    }

    // Calculate (x,y) coordinates of input value
    if ((nextSquare - 2*rowAndColOfSquare) < input)
    {
      // Value in given (bottom) row
      part1pt.x = +rowAndColOfSquare - (nextSquare - input);
      part1pt.y = +rowAndColOfSquare;
    }
    else if ((nextSquare - 4*rowAndColOfSquare) < input)
    {
      // Value in left vertical
      part1pt.x = -rowAndColOfSquare;
      part1pt.y = +rowAndColOfSquare - (nextSquare - 2*rowAndColOfSquare - input);
    }
    else if ((nextSquare - 6*rowAndColOfSquare) < input)
    {
      // Value in top row
      part1pt.x = -rowAndColOfSquare + (nextSquare - 4*rowAndColOfSquare - input);
      part1pt.y = -rowAndColOfSquare;
    }
    else
    {
      // Value in right vertical
      part1pt.x = +rowAndColOfSquare;
      part1pt.y = -rowAndColOfSquare + (nextSquare - 6*rowAndColOfSquare - input);
    }
  }

  printf("Part 1: %d\n", manhattan_distance(part1pt.x,part1pt.y, 0, 0)); // 430

}

void Day3Part2(std::string file)
{
  std::ifstream in(file);

  int input = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      input = std::stoi(line);
    }
  }

  int mapDim = std::ceil(input/1000);

  //        xmin   xmax   ymin   ymax
  Map map(-mapDim,+mapDim,-mapDim,+mapDim);

  // Initialize
  map.point(0,0)->setValue(1);

  // Start facing down since first iteration will rotate ccw
  std::array<int, 2> direction = {0, -1};

  std::array<int, 2> currentPoint = {0, 0};

  int lastValue = 0;
  for ( ; ; )
  {

    // Try to turn counterclockwise
    std::array<int,2> ccwDirection = rotation(Counterclockwise, direction);

    if (map.point(currentPoint[0] + ccwDirection[0],
                  currentPoint[1] + ccwDirection[1])->value == 0)
    {
      // Can turn now
      direction = ccwDirection;
    }
    else
    {
      // Continue in current direction
    }

    // Update position
    currentPoint = {currentPoint[0] + direction[0],
                    currentPoint[1] + direction[1]};
    
    Point* nextPoint = map.point(currentPoint[0],currentPoint[1]);

    // Set value of point
    std::vector<Point*> neighbors = map.getAllAdjacent(nextPoint);

    lastValue = sumNeighbors(neighbors);

    nextPoint->setValue(lastValue);

    if (lastValue >= input) {
      break;
    }
    
  }

  for (int y = +3; y >= -3; y--)
  {
    for (int x = -3; x <= +3; x++)
    {
      std::cout << " " << std::setw(5) << map.point(x,y)->value << " ";
    }
    std::cout << "\n";
  }

  printf("Part 2: %d\n", lastValue); // 312453
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day3Part1(argv[1]);

  Day3Part2(argv[1]);

  return 0;
}