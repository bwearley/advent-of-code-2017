#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <functional>
#include <map>

#include "../shared/stringtools.cpp"
//#include "../shared/math.cpp"

class Program
{
  public:
    int pid;
    std::vector<Program*> pipes;

    Program(int pid_in)
    {
      pid = pid_in;
    }
};

void Day12(std::string file)
{
  std::ifstream in(file);

  std::vector<std::string> inputStrings;

  std::vector<Program> programs;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      inputStrings.push_back(line);
    }
  }

  // Create programs
  for (auto & instrString : inputStrings)
  {
    auto strParts = split(instrString, ' ');
    int pid = std::stoi(strParts[0]);
    programs.push_back(Program(pid));
  }
  const int NUM_PROG = programs.size() + 1;

  // Assign pipes
  for (auto & instrString : inputStrings)
  {
    auto strParts = split(instrString, ' ');
    int pid = std::stoi(strParts[0]);
    for (auto i = 2; i != strParts.size(); i++)
    {
      int pipeToPid = std::stoi(strParts[i]);
      programs[pid].pipes.push_back(&programs[pipeToPid]);
    }
  }

  // Map of each network
  std::map<std::vector<bool>, int> map;

  // Initialize
  std::vector<bool> inThisNetwork;
  inThisNetwork.reserve(NUM_PROG);
  for (int j = 0; j < NUM_PROG; j++) { inThisNetwork.push_back(false); }

  // Recursive function to go through network
  std::function<void(Program&)> visitNetwork = [&](Program & prog) -> void
  {
    for (auto & pipe : prog.pipes)
    {
      if (!inThisNetwork[pipe->pid])
      {
        inThisNetwork[pipe->pid] = true;
        visitNetwork(*pipe);
      }
      else
      {
        inThisNetwork[pipe->pid] = true;
      }
    }
  };

  // Iterate through each program and generate its network
  for (auto i = 0; i < programs.size(); i++)
  {
    // Reset
    for (int j = 0; j < NUM_PROG; j++) { inThisNetwork[j] = false; }
    inThisNetwork[i] = true;

    // Visit network
    visitNetwork(programs[i]);

    // Part 1
    if (i == 0) {
      int part1 = 0;
      for (int j = 0; j < NUM_PROG; j++)
      {
        if (inThisNetwork[j]) part1 += 1;
      }
      printf("Part 1: %d\n", part1); // 169
    }

    auto freq = std::pair<std::vector<bool>, int>(inThisNetwork, 1);
    auto res = map.insert(freq);
    if (res.second == false) { res.first->second++; }
  }

  // Part 2
  printf("Part 2: %d\n", map.size()); // 179

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day12(argv[1]);

  return 0;
}