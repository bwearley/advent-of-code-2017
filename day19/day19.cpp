#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
//#include <functional>
//#include <map>

#include "../shared/stringtools.cpp"
//#include "../shared/math.cpp"

bool isLetter(char c)
{
  if (c == 'A' || c == 'B' || c == 'C' || c == 'D' || c == 'E' || c == 'F' ||
      c == 'G' || c == 'H' || c == 'I' || c == 'J' || c == 'K' || c == 'L' ||
      c == 'M' || c == 'N' || c == 'O' || c == 'P' || c == 'Q' || c == 'R' ||
      c == 'S' || c == 'T' || c == 'U' || c == 'V' || c == 'W' || c == 'X' ||
      c == 'Y' || c == 'Z') return true;
  return false;
}

void Day19(std::string file)
{
  std::ifstream in(file);

  std::vector<std::string> map;

  std::vector<char> part1;

  const int X = 0;
  const int Y = 1;
  int pos[] = { 0, 0 };
  int dir[] = { 0, 1 };

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      map.push_back(line);
    }
  }

  // Find starting position
  for (auto i = 0; i < map[0].size(); i++)
  {
    if (map[0][i] == '|') pos[0] = i;
  }

  // Debug
  //printf("Starting position: (%d,%d)\n",pos[X], pos[Y]);

  auto mapxy = [&](int p[2])
  {
    if (p[X] >= map[0].size() || p[Y] >= map.size()) return '*';
    return map[p[Y]][p[X]];
  };
 
  auto moveTo = [&](int p[2])
  {
    pos[X] = p[X];
    pos[Y] = p[Y];
  };

  auto turn = [&](int d[2])
  {
    dir[X] = d[X];
    dir[Y] = d[Y];
  };

  int steps = 1;
  for ( ; ; )
  {
    int next[] = { pos[X]+dir[X], pos[Y]+dir[Y] };
    
    // Turn
    if (mapxy(next) == '+')
    {
      moveTo(next);
      // Determine new direction (swap x,y and do both signs)
      int turn1[] = { +dir[Y], +dir[X] }; int turn1pos[] = { pos[X]+turn1[X], pos[Y]+turn1[Y] };
      int turn2[] = { -dir[Y], -dir[X] }; int turn2pos[] = { pos[X]+turn2[X], pos[Y]+turn2[Y] };
      if (mapxy(turn1pos) == '|' || mapxy(turn1pos) == '-' || isLetter(mapxy(turn1pos))) turn(turn1);
      if (mapxy(turn2pos) == '|' || mapxy(turn2pos) == '-' || isLetter(mapxy(turn2pos))) turn(turn2);
    }
    
    // Continue straight
    else if (mapxy(next) != ' ')
    {
      moveTo(next);
      if (isLetter(mapxy(next))) part1.push_back(mapxy(next));
    }
    
    // Done
    else
    {
      break;
    }
    steps += 1;
  }

  // Debug
  //printf("Final position: (%d,%d)\n", pos[X],pos[Y]);

  // Part 1 Output
  std::cout << "Part 1: "; // XYFDJNRCQA
  for (auto & c : part1)
  {
    std::cout << c ;
  }
  std::cout << std::endl;

  // Part 2 Output
  printf("Part 2: %d\n", steps); // 17450

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day19(argv[1]);

  return 0;
}