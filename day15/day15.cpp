#include <iostream>
#include <fstream>
#include <string>
#include <bitset>

#include "../shared/stringtools.cpp"

void Day15Part1(std::string file)
{
  std::ifstream in(file);

  long gen[] = {0, 0};
  long factors[] = { 16807, 48271 };
  const long divisor = 2147483647;

  const int A = 0;
  const int B = 1;

  const long cycles = 40000000;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      auto strValues = split(line, ' ');
      if (strValues[1] == "A") gen[A] = std::stoi(strValues[4]);
      if (strValues[1] == "B") gen[B] = std::stoi(strValues[4]);
    }
  }

  // Part 1 Processing
  long part1 = 0;
  for (long pair = 1; pair <= cycles; pair++)
  {

    gen[A] = (gen[A] * factors[A]) % divisor;
    gen[B] = (gen[B] * factors[B]) % divisor;

    auto binA = std::bitset<16>(gen[A]);
    auto binB = std::bitset<16>(gen[B]);

    if (binA == binB) part1+= 1;
  }

  printf("Part 1: %d\n", part1); // 594
}

void Day15Part2(std::string file)
{
  std::ifstream in(file);

  long gen[] = {0, 0};
  long factors[] = { 16807, 48271 };
  const long divisor = 2147483647;

  const int A = 0;
  const int B = 1;

  const long cycles = 5000000;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      auto strValues = split(line, ' ');
      if (strValues[1] == "A") gen[A] = std::stoi(strValues[4]);
      if (strValues[1] == "B") gen[B] = std::stoi(strValues[4]);
    }
  }

  // Generator A
  auto generatorA = [&](long in)
  {
    long out = in;
    for ( ; ; )
    {
      out = (out * factors[A]) % divisor;
      if (out % 4 == 0) return out;
    }
  };

  // Generator B
  auto generatorB = [&](long in)
  {
    long out = in;
    for ( ; ; )
    {
      out = (out * factors[B]) % divisor;
      if (out % 8 == 0) return out;
    }
  };

  // Part 2 Processing
  long part2 = 0;
  for (long pair = 1; pair <= cycles; pair++)
  {

    gen[A] = generatorA(gen[A]);
    gen[B] = generatorB(gen[B]);

    auto binA = std::bitset<16>(gen[A]);
    auto binB = std::bitset<16>(gen[B]);

    if (binA == binB) part2+= 1;
  }

  printf("Part 2: %d\n", part2); // 328
}
int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day15Part1(argv[1]);
  Day15Part2(argv[1]);

  return 0;
}