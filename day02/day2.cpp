#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../shared/stringtools.cpp"
#include "../shared/math.cpp"

void Day2(std::string file)
{
  std::ifstream in(file);

  // Part 1
  std::vector<int> rowDiffs;

  // Part 2
  std::vector<int> rowDivisions;

  int numRows = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      std::vector<int> valuesInt;

      // Get values as strings
      auto values = split(line, '\t'); // tab-delimited file
      
      // Convert strings to integers
      for (auto i = 0; i < values.size(); ++i)
      {
        valuesInt.emplace_back(std::stoi(values[i]));
      }

      // Part 1 - Calculate Row Diffs
      int rowDiff = max_value(valuesInt) - min_value(valuesInt);
      rowDiffs.emplace_back(rowDiff);

      // Part 2 - Calculate Result of Divisions of Row
      int rowDiv = 0;
      for (auto i = 0; i != valuesInt.size(); i++)
      {
        for (auto j = 0; j != valuesInt.size(); j++)
        {
          if (i == j) { continue; }
          // Dividing values found, save result
          if (valuesInt[i] % valuesInt[j] == 0) {
            rowDiv = valuesInt[i] / valuesInt[j];
            rowDivisions.emplace_back(rowDiv);
          }
        }
      }     
    }
    numRows += 1;
  }

  int checksum = sum(rowDiffs);
  int checksum2 = sum(rowDivisions);

  printf("Part 1: %d\n", checksum); // 58975
  printf("Part 2: %d\n", checksum2); // 308
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day2(argv[1]);

  return 0;
}