#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../shared/stringtools.cpp"

class Register
{
  public:
    int value;
    std::string name;

    Register(std::string name0)
    {
      name = name0;
      value = 0;
    }
};

enum InstructionCondition
{
  GreaterThan,
  LessThan,
  GreaterThanOrEqualTo,
  LessThanOrEqualTo,
  NotEqualTo,
  EqualTo,
};

enum InstructionAction
{
  Increment,
  Decrement,
};

class Instruction
{
  public:
    Register* targetReg;
    Register* conditionReg;
    InstructionAction action;
    InstructionCondition conditionType;
    int increment;
    int conditionValue;
};

void Day8(std::string file)
{
  std::ifstream in(file);

  std::vector<std::string> inputStrings;

  std::vector<Register> registers;
  std::vector<Instruction> instructions;

  // Lambda with capture by reference (&)
  auto addRegister = [&](std::string name)
  {
    registers.push_back(Register(name));
  };

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      inputStrings.push_back(line);
    }
  }

  // Create registers
  for (auto & instrString : inputStrings)
  {
    std::vector<std::string> strParts;
    strParts = split(instrString, ' ');

    // b inc 5 if a > 1
    std::string targetReg = strParts[0];
    std::string conditionReg = strParts[4];

    bool foundTargetReg = false;
    bool foundConditionReg = false;
    for (auto & reg : registers)
    {
      if (reg.name == targetReg)    { foundTargetReg = true;    }
      if (reg.name == conditionReg) { foundConditionReg = true; }
    }
    if (!foundTargetReg)    { addRegister(targetReg);    }
    if (!foundConditionReg) { addRegister(conditionReg); }
  }
  int numRegisters = registers.size();

  // Create instructions
  for (auto & instrString : inputStrings)
  {
    auto strParts = split(instrString, ' ');

    // 0  1  2 3  4 5 6
    // b inc 5 if a > 1
    auto targetReg     = strParts[0];
    auto instrAction   = strParts[1];
    auto instrIncrem   = strParts[2];
    auto conditionReg  = strParts[4];
    auto conditionType = strParts[5];
    auto condTestValue = strParts[6];

    /* Build New Instruction */
    Instruction newInstr;

    // Values
    newInstr.increment = std::stoi(instrIncrem);
    newInstr.conditionValue = std::stoi(condTestValue);

    // Action
    if (instrAction == "inc") { newInstr.action = Increment; }
    if (instrAction == "dec") { newInstr.action = Decrement; }
    
    // Condition Type
    if (conditionType == ">" ) { newInstr.conditionType = GreaterThan; }
    if (conditionType == ">=") { newInstr.conditionType = GreaterThanOrEqualTo; }
    if (conditionType == "<" ) { newInstr.conditionType = LessThan; }
    if (conditionType == "<=") { newInstr.conditionType = LessThanOrEqualTo; }
    if (conditionType == "!=") { newInstr.conditionType = NotEqualTo; }
    if (conditionType == "==") { newInstr.conditionType = EqualTo; }

    // Assign registers
    for (auto & reg : registers)
    {
      if (reg.name == targetReg)    { newInstr.targetReg    = &reg; }
      if (reg.name == conditionReg) { newInstr.conditionReg = &reg; }
    }

    // Add to the pile
    instructions.push_back(newInstr);
  }
  int numInstructions = instructions.size();
  
  // Evaluation loop
  int peakValue = 0;
  for (auto & instr : instructions)
  {
    // b inc 5 if a > 1

    // Decide whether to evaluate or not
    bool evaluate = false;
    switch (instr.conditionType)
    {
      case GreaterThan:
        if (instr.conditionReg->value >  instr.conditionValue) { evaluate = true; }
        break;
      case GreaterThanOrEqualTo:
        if (instr.conditionReg->value >= instr.conditionValue) { evaluate = true; }
        break;
      case LessThan:
        if (instr.conditionReg->value <  instr.conditionValue) { evaluate = true; }
        break;
      case LessThanOrEqualTo:
        if (instr.conditionReg->value <= instr.conditionValue) { evaluate = true; }
        break;
      case NotEqualTo:
        if (instr.conditionReg->value != instr.conditionValue) { evaluate = true; }
        break;
      case EqualTo:
        if (instr.conditionReg->value == instr.conditionValue) { evaluate = true; }
        break;
      default:
        std::cout << "ERROR: Unknown condition type." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Evaluate
    if (evaluate)
    {
      switch (instr.action)
      {
        case Increment:
          instr.targetReg->value += instr.increment;
          break;
        case Decrement:
          instr.targetReg->value -= instr.increment;
          break;
        default:
          std::cout << "ERROR: Unknown action type." << std::endl;
          exit(EXIT_FAILURE);
      }
    }

    // Part 2: keep track of maximum value
    if (instr.targetReg->value > peakValue) peakValue = instr.targetReg->value;
  }

  // Part 1: get maximum value at end of loop
  int maxvalue = 0;
  for (auto & reg : registers)
  {
    if (reg.value > maxvalue) maxvalue = reg.value;
  }

  // Outputs
  printf("Part 1: %d\n", maxvalue); // 6828
  printf("Part 2: %d\n", peakValue); // 7234

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day8(argv[1]);

  return 0;
}