#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../shared/stringtools.cpp"

class Layer
{
  public:
    int depth;
    int range;
    int scannerPos;
    int direction;

    Layer(int depth_in, int range_in)
    {
      depth = depth_in;
      range = range_in;
      scannerPos = 1;
      direction = +1;
    }

    void advanceScanner()
    {
      if (scannerPos == range) direction = -1;
      if (scannerPos == 1    ) direction = +1;
      scannerPos += direction;
    }

    bool caughtAtTime(int time)
    {
      return time % (2*(range-1)) == 0;
    }
};

void Day13Part1(std::string file)
{
  std::ifstream in(file);

  std::vector<Layer> layers;
  layers.reserve(100);

  int lastLayerDepth = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      auto values = split(line, ':');

      int depth = std::stoi(values[0]);
      int range = std::stoi(values[1]);
      layers.push_back(Layer(depth, range));

      lastLayerDepth = depth;
    }
  }

  // Build vector of layer addresses (premature optimization)
  Layer* layerAddr[lastLayerDepth+1];
  for (auto i = 0; i <= lastLayerDepth; i++)
  {
    layerAddr[i] = NULL;
    for (auto & l : layers)
    {
      if (l.depth == i)
      {
        layerAddr[i] = &l;
        break;
      }
    }
  }

  // Time loop
  int packetPos = 0;
  int severity = 0;
  for (int t = 0; t <= lastLayerDepth; t++)
  {
    if (layerAddr[packetPos] != NULL)
    {
      if (layerAddr[packetPos]->scannerPos == 1)
      {
        severity += (layerAddr[packetPos]->depth * layerAddr[packetPos]->range);
      }
    }
    // Advance
    packetPos += 1;
    for (auto & layer : layers) { layer.advanceScanner(); }
  }
  printf("Part 1: %d\n", severity); // 1704
}

void Day13Part2(std::string file)
{
  std::ifstream in(file);

  std::vector<Layer> layers;
  layers.reserve(100);

  int lastLayerDepth = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      auto values = split(line, ':');

      int depth = std::stoi(values[0]);
      int range = std::stoi(values[1]);
      layers.push_back(Layer(depth, range));

      lastLayerDepth = depth;
    }
  }

  // Build vector of layer addresses 
  Layer* layerAddr[lastLayerDepth+1];
  for (auto i = 0; i <= lastLayerDepth; i++)
  {
    layerAddr[i] = NULL;
    for (auto & l : layers)
    {
      if (l.depth == i)
      {
        layerAddr[i] = &l;
        break;
      }
    }
  }

  // Time loop
  int packetPos = 0;
  int wait = 0;
  bool caught = false;
  for ( ; ; wait++)
  {
    packetPos = 0;
    caught = false;
    //for (auto & layer : layers) { layer.scannerPos = 1; }
    /*for (int t = 0; t < wait; t++)
    {
      for (auto & layer : layers) { layer.advanceScanner(); }
    }*/

    for (int t = 0; t <= lastLayerDepth; t++)
    {
      if (layerAddr[packetPos] != NULL)
      {
        //if (layerAddr[packetPos]->scannerPos == 1)
        if (layerAddr[packetPos]->caughtAtTime(t+wait))
        {
          caught = true;
          break;
        }
      }

      // Advance
      packetPos += 1;
      //for (auto & layer : layers) { layer.advanceScanner(); }
    }
    if (!caught) break;
  }
  printf("Part 2: %d\n", wait); // 3970918
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day13Part1(argv[1]);
  Day13Part2(argv[1]);

  return 0;
}