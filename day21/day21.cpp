#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <boost/optional.hpp>

#include "../shared/stringtools.cpp"
#include "../shared/map.cpp"


class EnhancementRule
{
  public:
    int size_in;
    int size_out;
    std::string input;
    std::string in;
    std::string out;
    std::vector<std::string> rows_in;
    std::vector<std::string> rows_out;
    //std::vector<std::string> cols;

    boost::optional<Map> inputMap;
    boost::optional<Map> outputMap;

    EnhancementRule(std::string input)
    {
      auto parts = split(input,'=');

      // Store in string
      in = trim(parts[0]);

      // Generate rows and columns of input
      rows_in = split(in,'/');
      size_in = rows_in.size();

      // Generate input map
      inputMap = Map(0,size_in,0,+size_in);
      for (auto y = 0; y != size_in; y++)
      {
        for (auto x = 0; x != size_in; x++)
        {
          if (rows_in[y][x] == '#')
          {
            inputMap->point(x,y)->setValue(1);
          }
          else
          {
            inputMap->point(x,y)->setValue(0);
          }
        }
      }

      // Store out string
      parts[1].erase(parts[1].begin()); // Eliminate leading "> "
      out = trim(parts[1]);

      // Generate rows and columns of output
      rows_out = split(out,'/');
      size_out = rows_out.size();

      // Generate output map
      outputMap = Map(0,size_out,0,+size_out);
      for (auto y = 0; y != size_out; y++)
      {
        for (auto x = 0; x != size_out; x++)
        {
          if (rows_out[y][x] == '#')
          {
            outputMap->point(x,y)->setValue(1);
          }
          else
          {
            outputMap->point(x,y)->setValue(0);
          }
        }
      }
    }

    EnhancementRule flipL2R()
    {
      EnhancementRule selfCopy = *this;
      for (auto y = 0; y != size_in; y++)
      {
        int x2 = this->size_in - 1;
        for (auto x = 0; x != size_in; x++)
        {
          selfCopy.inputMap->point(x,y)->setValue(
            this->inputMap->point(x2,y)->value
          );
          x2 -= 1;
        }
      }
      return selfCopy;
    }

    EnhancementRule flipT2B()
    {
      EnhancementRule selfCopy = *this;
      for (auto x = 0; x != size_in; x++)
      {
        int y2 = this->size_in - 1;
        for (auto y = 0; y != size_in; y++)
        {
          selfCopy.inputMap->point(x,y)->setValue(
            this->inputMap->point(x,y2)->value
          );
          y2 -= 1;
        }
      }
      return selfCopy;
    }

    EnhancementRule rotate(int angle)
    {
      EnhancementRule selfCopy = *this;
      for (auto rots = 1; rots <= angle/90; rots++)
      {
        EnhancementRule pastCopy = selfCopy;
        switch (selfCopy.size_in)
        {
          case 2:
            selfCopy.inputMap->point(0,0)->setValue(
              pastCopy.inputMap->point(1,0)->value
            );
            selfCopy.inputMap->point(0,1)->setValue(
              pastCopy.inputMap->point(0,0)->value
            );
            selfCopy.inputMap->point(1,0)->setValue(
              pastCopy.inputMap->point(1,1)->value
            );
            selfCopy.inputMap->point(1,1)->setValue(
              pastCopy.inputMap->point(0,1)->value
            );
            break;
          case 3:
            selfCopy.inputMap->point(0,0)->setValue(
              pastCopy.inputMap->point(2,0)->value
            );
            selfCopy.inputMap->point(0,1)->setValue(
              pastCopy.inputMap->point(1,0)->value
            );
            selfCopy.inputMap->point(0,2)->setValue(
              pastCopy.inputMap->point(0,0)->value
            );
            selfCopy.inputMap->point(1,0)->setValue(
              pastCopy.inputMap->point(2,1)->value
            );
            selfCopy.inputMap->point(1,1)->setValue(
              pastCopy.inputMap->point(1,1)->value
            );
            selfCopy.inputMap->point(1,2)->setValue(
              pastCopy.inputMap->point(0,1)->value
            );
            selfCopy.inputMap->point(2,0)->setValue(
              pastCopy.inputMap->point(2,2)->value
            );
            selfCopy.inputMap->point(2,1)->setValue(
              pastCopy.inputMap->point(1,2)->value
            );
            selfCopy.inputMap->point(2,2)->setValue(
              pastCopy.inputMap->point(0,2)->value
            );
            break;
          default:
            std::cout << "ERROR: EnhancementRule::rotate() not implemented for sizes above 3." << std::endl;
            break;
        }
      }
      return selfCopy;
    }

};

void Day21(std::string file)
{
  std::ifstream in(file);

  std::vector<EnhancementRule> rules;

  const int part1iter = 5;
  const int part2iter = 18;

  int part1ans = 0;
  int part2ans = 0;

  int NUM_ITERS = 18;
  if (file == "test.txt") NUM_ITERS = 2;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      rules.push_back(EnhancementRule(line));
    }
  }

  // Generate rotations
  int currentLimit = rules.size();
  for (auto i = 0; i < currentLimit; i++)
  {
    rules.push_back(rules[i].rotate(90));
    rules.push_back(rules[i].rotate(180));
    rules.push_back(rules[i].rotate(270));
  }

  // Generate flips
  currentLimit = rules.size();
  for (auto i = 0; i < currentLimit; i++)
  {
    rules.push_back(rules[i].flipL2R());
    //rules.push_back(rules[i].flipT2B()); // Redundant
  }

  // Print Maps
  // for (auto & r : rules)
  // {
  //   for (auto y = 0; y < r.size_in; y++)
  //   {
  //     for (auto x = 0; x < r.size_in; x++)
  //     {
  //       if (r.inputMap->point(x,y)->value == 1) { std::cout << "#"; }
  //       else { std::cout << "."; }
  //     }
  //     std::cout << std::endl;
  //   }
  //   std::cout << std::endl;
  // }

  // Generate map
  const int xmax = 2500;
  const int ymax = 2500;
  Map map(0,xmax,0,ymax);

  // Apply starting pattern
  int size = 3;
  map.point(0,0)->setValue(0);
  map.point(1,0)->setValue(1);
  map.point(2,0)->setValue(0);
  map.point(0,1)->setValue(0);
  map.point(1,1)->setValue(0);
  map.point(2,1)->setValue(1);
  map.point(0,2)->setValue(1);
  map.point(1,2)->setValue(1);
  map.point(2,2)->setValue(1);

  auto numberLitPixels = [&]()
  {
    int numLit = 0;
    for (auto y = 0; y < size; y++)
    {
      for (auto x = 0; x < size; x++)
      {
        if (map.point(x,y)->value == 1) numLit += 1;
      }
    }
    return numLit;
  };

  auto printMap = [&]()
  {
    for (auto y = 0; y < size; y++)
    {
      for (auto x = 0; x < size; x++)
      {
        if (map.point(x,y)->value == 1)
        {
          std::cout << "#";
        }
        else
        {
          std::cout << ".";
        }
      }
      std::cout << std::endl;
    }
  };

  // Debug
  //printf("Initial State (Lit:%d, Size: %d)\n", numberLitPixels(), size);
  //printMap();

  for (int iter = 1; iter <= NUM_ITERS; iter++)
  {
    Map mapBefore = map;

    if (size % 2 == 0)
    {
      // Break map into 2x2 squares
      for (auto blkY = 0; blkY < size/2; blkY++)
      {
        for (auto blkX = 0; blkX < size/2; blkX++)
        {
          Map minimap = Map(0,2,0,2);
          minimap.point(0,0)->setValue(mapBefore.point(blkX*2+0,blkY*2+0)->value);
          minimap.point(0,1)->setValue(mapBefore.point(blkX*2+0,blkY*2+1)->value);
          minimap.point(1,0)->setValue(mapBefore.point(blkX*2+1,blkY*2+0)->value);
          minimap.point(1,1)->setValue(mapBefore.point(blkX*2+1,blkY*2+1)->value);

          bool found = false;
          for (auto & rule : rules)
          {
            if (rule.inputMap->isEquivalent(minimap))
            {
              found = true;              
              // Do replacement
              map.point(blkX*3+0,blkY*3+0)->setValue(rule.outputMap->point(0,0)->value);
              map.point(blkX*3+0,blkY*3+1)->setValue(rule.outputMap->point(0,1)->value);
              map.point(blkX*3+0,blkY*3+2)->setValue(rule.outputMap->point(0,2)->value);
              map.point(blkX*3+1,blkY*3+0)->setValue(rule.outputMap->point(1,0)->value);
              map.point(blkX*3+1,blkY*3+1)->setValue(rule.outputMap->point(1,1)->value);
              map.point(blkX*3+1,blkY*3+2)->setValue(rule.outputMap->point(1,2)->value);
              map.point(blkX*3+2,blkY*3+0)->setValue(rule.outputMap->point(2,0)->value);
              map.point(blkX*3+2,blkY*3+1)->setValue(rule.outputMap->point(2,1)->value);
              map.point(blkX*3+2,blkY*3+2)->setValue(rule.outputMap->point(2,2)->value);
              break;
            }
          }
          if (!found) { printf("ERROR: No matching 2x2 rule found for block (%d,%d), iter=%d\n",blkX,blkY,iter); return; }
        }
      }
      size = (size / 2) * 3;
    }
    else
    {
      // Break map into 3x3 squares
      for (auto blkY = 0; blkY < size/3; blkY++)
      {
        for (auto blkX = 0; blkX < size/3; blkX++)
        {
          Map minimap = Map(0,3,0,3);
          minimap.point(0,0)->setValue(mapBefore.point(blkX*3+0,blkY*3+0)->value);
          minimap.point(0,1)->setValue(mapBefore.point(blkX*3+0,blkY*3+1)->value);
          minimap.point(0,2)->setValue(mapBefore.point(blkX*3+0,blkY*3+2)->value);
          minimap.point(1,0)->setValue(mapBefore.point(blkX*3+1,blkY*3+0)->value);
          minimap.point(1,1)->setValue(mapBefore.point(blkX*3+1,blkY*3+1)->value);
          minimap.point(1,2)->setValue(mapBefore.point(blkX*3+1,blkY*3+2)->value);
          minimap.point(2,0)->setValue(mapBefore.point(blkX*3+2,blkY*3+0)->value);
          minimap.point(2,1)->setValue(mapBefore.point(blkX*3+2,blkY*3+1)->value);
          minimap.point(2,2)->setValue(mapBefore.point(blkX*3+2,blkY*3+2)->value);

          bool found = false;
          for (auto & rule : rules)
          {
            if (rule.inputMap->isEquivalent(minimap))
            {
              found = true;
              // Do replacement
              map.point(blkX*4+0,blkY*4+0)->setValue(rule.outputMap->point(0,0)->value);
              map.point(blkX*4+0,blkY*4+1)->setValue(rule.outputMap->point(0,1)->value);
              map.point(blkX*4+0,blkY*4+2)->setValue(rule.outputMap->point(0,2)->value);
              map.point(blkX*4+0,blkY*4+3)->setValue(rule.outputMap->point(0,3)->value);

              map.point(blkX*4+1,blkY*4+0)->setValue(rule.outputMap->point(1,0)->value);
              map.point(blkX*4+1,blkY*4+1)->setValue(rule.outputMap->point(1,1)->value);
              map.point(blkX*4+1,blkY*4+2)->setValue(rule.outputMap->point(1,2)->value);
              map.point(blkX*4+1,blkY*4+3)->setValue(rule.outputMap->point(1,3)->value);

              map.point(blkX*4+2,blkY*4+0)->setValue(rule.outputMap->point(2,0)->value);
              map.point(blkX*4+2,blkY*4+1)->setValue(rule.outputMap->point(2,1)->value);
              map.point(blkX*4+2,blkY*4+2)->setValue(rule.outputMap->point(2,2)->value);
              map.point(blkX*4+2,blkY*4+3)->setValue(rule.outputMap->point(2,3)->value);

              map.point(blkX*4+3,blkY*4+0)->setValue(rule.outputMap->point(3,0)->value);
              map.point(blkX*4+3,blkY*4+1)->setValue(rule.outputMap->point(3,1)->value);
              map.point(blkX*4+3,blkY*4+2)->setValue(rule.outputMap->point(3,2)->value);
              map.point(blkX*4+3,blkY*4+3)->setValue(rule.outputMap->point(3,3)->value);
              break;
            }
          }
          if (!found) { printf("ERROR: No matching 3x3 rule found for block (%d,%d), iter=%d\n",blkX,blkY,iter); return; }
        }
      }
      size = (size / 3) * 4;
    }

    // Debug
    //printf("Iteration %d (Lit: %d, Size: %d)\n",iter,numberLitPixels(),size);
    //printMap();

    // Answer Outputs
    if (iter == part1iter) part1ans = numberLitPixels();
    if (iter == part2iter) part2ans = numberLitPixels();
  }

  printf("Part 1: %d\n", part1ans); // 179
  printf("Part 2: %d\n", part2ans); // 2766750

  printf("Final size: %d\n", size);
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day21(argv[1]);

  return 0;
}