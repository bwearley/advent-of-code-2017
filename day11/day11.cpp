#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>

#include "../shared/stringtools.cpp"
//#include "../shared/math.cpp"

/*
  Hexagonal Grids
  * Explanation at https://www.redblobgames.com/grids/hexagons/
 */

const int X = 0;
const int Y = 1;
const int Z = 2;

int hex_distance(int pos[3])
{
  return (abs(pos[X]) + abs(pos[Y]) + abs(pos[Z])) / 2;
}

void Day11(std::string file)
{
  std::ifstream in(file);

  int position[3] = { 0, 0, 0 };

  int part2 = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      std::vector<std::string> values;

      // Get values as strings
      values = split(line, ','); // tab-delimited file
      
      // Convert strings to direction differentials
      for (auto & dir : values)
      {
        //printf("%s\n",dir.c_str());
        if (dir == "n")
        {
          position[Y] += 1;
          position[Z] -= 1;
        }
        else if (dir == "s")
        {
          position[Y] -= 1;
          position[Z] += 1;
        }
        else if (dir == "nw")
        {
          position[X] -= 1;
          position[Y] += 1;
        }
        else if (dir == "ne")
        {
          position[X] += 1;
          position[Z] -= 1;
        }
        else if (dir == "sw")
        {
          position[X] -= 1;
          position[Z] += 1;
        }
        else if (dir == "se")
        {
          position[X] += 1;
          position[Y] -= 1;
        }
        else
        {
          printf("ERROR: Unknown direction: %s\n", dir.c_str());
          exit(EXIT_FAILURE);
        }
        part2 = std::max(part2, hex_distance(position));
      }    
    }
  }

  printf("Part 1: %d\n", hex_distance(position)); // 720
  printf("Part 2: %d\n", part2); // 1485
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day11(argv[1]);

  return 0;
}