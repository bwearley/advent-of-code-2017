#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <regex>
#include <algorithm>

#include "../shared/stringtools.cpp"
#include "../shared/vec3d.cpp"

class Particle
{
  public:
    int id;
    Vec3D pos;
    Vec3D vel;
    Vec3D acc;
    bool alive;

    Particle(int id0, std::string in)
    {

      // Initialize
      id = id0;
      alive = true;

      // Extract numbers from input strings
      int64_t state0[9];
      std::regex regex("[-*\\d]+");
      auto numbers = std::sregex_iterator(in.begin(), in.end(), regex);
      int j = 0;
      for (auto i = numbers; i != std::sregex_iterator(); i++)
      {
        std::smatch n = *i;
        state0[j++] = std::stoi(n.str());
      }
      pos.x = state0[0];
      pos.y = state0[1];
      pos.z = state0[2];
      vel.x = state0[3];
      vel.y = state0[4];
      vel.z = state0[5];
      acc.x = state0[6];
      acc.y = state0[7];
      acc.z = state0[8];
    }

    void printRepresentation()
    {
      printf("Particle %d, p=<%lld,%lld,%lld>, v=<%lld,%lld,%lld>, a=<%lld,%lld,%lld>\n", id,
        pos.x,pos.y,pos.z,
        vel.x,vel.y,vel.z,
        acc.x,acc.y,acc.z);
    }

    int64_t distOrigin()
    {
      return std::abs(pos.x) + std::abs(pos.y) + std::abs(pos.z);
    }

    void update()
    {
      vel = vel + acc;
      pos = pos + vel;
    }
};

void Day20Part1(std::string file)
{
  std::ifstream in(file);

  std::vector<Particle> particles;

  // Load file data
  int id = 0;
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      particles.push_back(Particle(id++,line));
    }
  }

  Particle* closest = &particles[0];
  auto closestOriginDistance = [&]()
  {
    for (auto & p : particles)
    {
      if (p.distOrigin() < closest->distOrigin())
      {
        closest = &p;
      }
    }
  };

  auto updatePositions = [&]()
  {
    for (auto & p : particles)
    {
      p.update();
    }
  };

  int64_t tick = 0;
  for ( ; ; )
  {
    updatePositions();
    closestOriginDistance();
    if (tick == 1000) break;
    tick += 1;
  }

  // Part 1
  printf("Part 1: %d (%lld)\n", closest->id, closest->distOrigin()); // 376
}

void Day20Part2(std::string file)
{
  std::ifstream in(file);

  std::vector<Particle> particles;

  // Load file data
  int id = 0;
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      particles.push_back(Particle(id++,line));
    }
  }

  auto updatePositions = [&]()
  {
    for (auto & p : particles)
    {
      p.update();
    }
  };
    
  struct ParticleMap
  {
    Vec3D pos;
    std::vector<int> partIDs;
    ParticleMap(Vec3D p, int newId)
    {
      pos = p;
      partIDs.push_back(newId);
    }
  };

  auto collideParticles = [&]()
  {
    std::vector<ParticleMap> seen;
    // Collect particle positions
    for (auto & p : particles)
    {
      if (!p.alive) continue; // Skip already-collided particles

      bool seenBefore = false;
      for (auto & s : seen)
      {
        if (s.pos == p.pos)
        {
          seenBefore = true;
          s.partIDs.push_back(p.id);
          break;
        }
      }
      if (!seenBefore) {
        seen.push_back(
          ParticleMap(p.pos, p.id)
        );
      }
    }

    // Kill collided particles
    for (auto & s : seen)
    {
      if (s.partIDs.size() > 1)
      {
        for (auto & sID : s.partIDs)
        {
          particles[sID].alive = false;
        }
      }
    }
  };

  int64_t tick = 0;
  for ( ; ; )
  {
    updatePositions();
    collideParticles();
    if (tick == 1000) break;
    tick += 1;
  }

  // Part 2
  int part2 = 0;
  for (auto & p : particles) { if (p.alive) part2 += 1; }
  printf("Part 2: %d\n", part2); // 574
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day20Part1(argv[1]);
  Day20Part2(argv[1]);

  return 0;
}