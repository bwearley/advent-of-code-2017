#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <utility>

#include "../shared/stringtools.cpp"
#include "../shared/math.cpp"

class Passphrase
{
  public:
    std::string phrase;
    std::vector<std::string> words;
    std::map<std::string, int> map;
    std::vector<std::map<char, int>> charMaps;
    bool valid;

    Passphrase(std::string phrase)
    {
      // Get individual words
      words = split(phrase, ' ');

      // Calculate word map
      for (auto & word : words)
      {
        auto thisWordFreq = std::pair<std::string, int>(word, 1);
        auto res = map.insert(thisWordFreq);
        // If insertion points to second matching key on map, increment
        if (res.second == false) { res.first->second++; }
      }

      // Detremine validity
      valid = true;
      for (auto & elem : map)
      {
        if (elem.second > 1)
        {
          valid = false;
          break;
        }
      }
    }

    void applySecondaryValidtyCriteria()
    {
      if (!valid) { return; }

      // Generate map of frequency of each letter in each word
      for (auto & word : words)
      {
        std::map<char, int> thisWordCharMap;
        for (char const & c: word)
        {
          auto thisCharFreq = std::pair<char, int>(c, 1);
          auto res = thisWordCharMap.insert(thisCharFreq);
          // If insertion points to second matching key on map, increment
          if (res.second == false) { res.first->second++; }
        }
        charMaps.push_back(thisWordCharMap);
      }

      // Compare maps
      for (auto i = 0; i <= charMaps.size(); i++)
      {
        for (auto j = 0; j <= charMaps.size(); j++)
        {
          if (i == j) { continue; }
          if (charMaps[i] == charMaps[j]) { valid = false; break; }
        }
      }
    }
};

void Day4(std::string file)
{
  std::ifstream in(file);

  // Input strings
  std::vector<std::string> passphraseStrings;

  std::vector<Passphrase> passphrases;

  int numRows = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      // Save strings
      passphraseStrings.emplace_back(line);
    }
    numRows += 1;
  }
  printf("Number of passphrases: %d\n", numRows);

  // Create Passphrase objects
  for (auto & passphrase : passphraseStrings)
  {
    passphrases.push_back(Passphrase(passphrase));
  }

  // Count invalid passphrases
  int numInvalid = 0;
  for (auto & p : passphrases)
  {
    if (!p.valid)
    {
      numInvalid += 1;
    }
  }

  printf("Part 1: %d\n", numRows - numInvalid); // 466

  // Part 2
  numInvalid = 0;
  for (auto & p : passphrases)
  {
    p.applySecondaryValidtyCriteria();
    if (!p.valid)
    {
      numInvalid += 1;
    }
  }

  printf("Part 2: %d\n", numRows - numInvalid); // 251
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day4(argv[1]);

  return 0;
}