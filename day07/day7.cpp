#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <map>

#include "../shared/stringtools.cpp"

class Program
{
  public:
    std::string name;
    int weight;
    int diskWeight;
    std::vector<std::string> childrenNames;
    std::vector<Program *> children;
    Program * parent = NULL;

    Program(std::string str)
    {
      // rvqrjoq (97)
      // mjvue (137) -> zgklk, lrjhm
      // axtlgbv (56) -> iyfxetp, nalyti, ufvts, yjlqjsb
      str.erase(std::remove(str.begin(), str.end(), '('), str.end());
      str.erase(std::remove(str.begin(), str.end(), ')'), str.end());
      str.erase(std::remove(str.begin(), str.end(), ','), str.end());

      auto stringParts = split(str, ' '); // axtlgbv 56 -> iyfxetp nalyti ufvts yjlqjsb

      name = stringParts[0];
      weight = std::stoi(stringParts[1]);

      // Has children
      if (stringParts.size() > 2)
      {
        for (auto i = 3; i < stringParts.size(); i++)
        {
          childrenNames.push_back(stringParts[i]);
        }
      }
    }

    void getDiskWeights()
    {
      if (children.size() == 0)
      {
        diskWeight = weight;
      }
      else
      {
        diskWeight = 0;
        for (auto & subprog : children)
        {
          subprog->getDiskWeights();
          diskWeight += subprog->diskWeight;
        }
        diskWeight += weight;
      }
    }
};

void Day7(std::string file)
{
  std::ifstream in(file);

  std::vector<Program> progs;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      progs.push_back(Program(line));  
    }
  }

  // Assign parent/child pointers
  for (auto & prog : progs)
  {
    for (auto & chName : prog.childrenNames)
    {
      for (auto & otherProg : progs)
      {
        if (otherProg.name == chName)
        {
          prog.children.push_back(&otherProg); // Assign child
          otherProg.parent = &prog; // Assign parent
        }
      }
    }
  }

  // Part 1 - find program with no parents
  Program *main = NULL;
  for (auto & prog : progs)
  {
    if (prog.parent == NULL)
    {
      std::cout << "Part 1: " << prog.name << std::endl; // qibuqqg
      main = &prog;
    }
  }

  // Part 2 - disc weights
  main->getDiskWeights();

  // Traverse up the tree to find imbalance
  Program * currentProg = main;
  int aberrant = 0;
  int lastGoodWeight = 0;
  int lastGoodWeight0 = 0;
  int lastBadWeight = 0;
  for ( ; ; )
  {
    std::vector<int> currProgWeights;

    // Collect weights of this program's children
    for (auto & child : currentProg->children)
    {
      currProgWeights.push_back(child->diskWeight);
    }

    // Collect frequencies of each weight
    std::map<int, int> weightsMap;
    for (auto & w : currProgWeights)
    {
      auto freq = std::pair<int, int>(w, 1);
      auto res = weightsMap.insert(freq);
      if (res.second == false) { res.first->second++; }
    }

    // Identify which weight is aberrant
    aberrant = 0;
    for (auto it = weightsMap.begin(); it != weightsMap.end(); it++)
    {
      if (it->second == 1)
      {
        aberrant = it->first;
      }
      else
      {
        lastGoodWeight0 = it->first;
      }
    }
    
    // Break if done
    if (aberrant == 0) break;
    lastBadWeight = aberrant;
    lastGoodWeight = lastGoodWeight0;
    
    // Move up tree if continuing
    for (auto & child : currentProg->children)
    {
      if (child->diskWeight == aberrant)
      {
        currentProg = child;
        break;
      }
    }
  }

  printf("Part 2: %d\n", currentProg->weight - (lastBadWeight - lastGoodWeight)); // 1079

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day7(argv[1]);

  return 0;
}