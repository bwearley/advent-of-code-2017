#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>

enum MachineState
{
  StateA,
  StateB,
  StateC,
  StateD,
  StateE,
  StateF,
};

void Day25()
{

  const int NUM_STEPS = 12302209;

  MachineState state = StateA;

  // Initialize Tape
  const int right = +1;
  const int left  = -1;
  const int TAPE_LEN = 100000;
  int tape[TAPE_LEN] = { 0 };

  int cursor = TAPE_LEN/2;

  for (auto step = 0; step < NUM_STEPS; step++)
  {
    switch(state)
    {
      case StateA:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 1;
          cursor += right;
          state = StateB;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 0;
          cursor += left;
          state = StateD;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      case StateB:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 1;
          cursor += right;
          state = StateC;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 0;
          cursor += right;
          state = StateF;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      case StateC:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 1;
          cursor += left;
          state = StateC;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 1;
          cursor += left;
          state = StateA;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      case StateD:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 0;
          cursor += left;
          state = StateE;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 1;
          cursor += right;
          state = StateA;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      case StateE:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 1;
          cursor += left;
          state = StateA;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 0;
          cursor += right;
          state = StateB;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      case StateF:
        if (tape[cursor] == 0)
        {
          tape[cursor] = 0;
          cursor += right;
          state = StateC;
        }
        else if (tape[cursor] == 1)
        {
          tape[cursor] = 0;
          cursor += right;
          state = StateE;
        }
        else { printf("ERROR: Unknown value.\n"); exit(EXIT_FAILURE);} 
        break;

      default:
        std::cout << "ERROR: Unknown state: " << state << std::endl;
        exit(EXIT_FAILURE);
        break;
    }

    if (cursor >= TAPE_LEN || cursor < 0) { printf("ERROR: ");exit(EXIT_FAILURE);}
  }

  int part1;
  for (auto i = 0; i < TAPE_LEN; i++)
  {
    part1 += tape[i];
  }
  printf("Part 1: %d\n", part1); // 633

}

int main(int argc, char** argv)
{

  Day25();

  return 0;
}