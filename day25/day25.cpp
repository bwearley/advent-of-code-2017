#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>

enum MachineState
{
  StateA,
  StateB,
  StateC,
  StateD,
  StateE,
  StateF,
};

struct Machine
{
  MachineState state;
  int writeValue0;
  int cursorShift0;
  MachineState newState0;
  int writeValue1;
  int cursorShift1;
  MachineState newState1;
};

void Day25()
{

  const int NUM_STEPS = 12302209;

  // Initialize Tape
  const int right = +1;
  const int left  = -1;
  const int TAPE_LEN = 100000;
  int tape[TAPE_LEN] = { 0 };

  int cursor = TAPE_LEN/2; // Start in the middle

  //                   (writeValue,Direction,newState)
  //     Mach   Mach   <---curr=0--->  <---curr=1--->
  Machine A = {StateA, 1,right,StateB, 0,left ,StateD, };
  Machine B = {StateB, 1,right,StateC, 0,right,StateF, };
  Machine C = {StateC, 1,left ,StateC, 1,left ,StateA, };
  Machine D = {StateD, 0,left ,StateE, 1,right,StateA, };
  Machine E = {StateE, 1,left ,StateA, 0,right,StateB, };
  Machine F = {StateF, 0,right,StateC, 0,right,StateE, };

  Machine *state = &A;

  for (auto step = 0; step < NUM_STEPS; step++)
  {
    if (tape[cursor] == 0)
    { 
      tape[cursor] = state->writeValue0;
      cursor += state->cursorShift0;
      switch(state->newState0)
      {
        case StateA: state = &A; break;
        case StateB: state = &B; break;
        case StateC: state = &C; break;
        case StateD: state = &D; break;
        case StateE: state = &E; break;
        case StateF: state = &F; break;
        default: break;
      }
    }
    else
    {
      tape[cursor] = state->writeValue1;
      cursor += state->cursorShift1;
      switch(state->newState1)
      {
        case StateA: state = &A; break;
        case StateB: state = &B; break;
        case StateC: state = &C; break;
        case StateD: state = &D; break;
        case StateE: state = &E; break;
        case StateF: state = &F; break;
        default: break;
      }
    }
  }

  int part1;
  for (auto i = 0; i < TAPE_LEN; i++)
  {
    part1 += tape[i];
  }
  printf("Part 1: %d\n", part1); // 633
}

int main(int argc, char** argv)
{
  Day25();
  return 0;
}