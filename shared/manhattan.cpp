#include <iostream>
#include <cstdlib>

int manhattan_distance(const int x0, const int y0, const int x1, const int y1)
{
  return abs(x1-x0) + abs(y1-y0);
}
int manhattan_distance(const int x0, const int y0, const int z0, const int x1, const int y1, const int z1)
{
  return abs(x1-x0) + abs(y1-y0) + abs(z1-z0);
}