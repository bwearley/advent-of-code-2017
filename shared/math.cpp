#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

int max_value(const std::vector<int>& vec)
{
  auto res = std::max_element(vec.begin(), vec.end());
  auto ix = std::distance(vec.begin(), res);
  return vec[ix];
}
int min_value(const std::vector<int>& vec)
{
  auto res = std::min_element(vec.begin(), vec.end());
  auto ix = std::distance(vec.begin(), res);
  return vec[ix];
}
int sum(const std::vector<int>& vec)
{
  return std::accumulate(vec.begin(), vec.end(), 0);
}