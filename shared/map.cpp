#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

enum MapDimension
{
  Map2D,
  Map3D
};

class Point
{
  public:
    int x, y, z;
    int value;
    bool visited;

    Point(int x0=0, int y0=0, int z0=0, int val=0)
    //Point(int x0, int y0, int z0=0, int val=0) : x{x0}, y{y0}, z{z0}, value{val}
    {
      x = x0;
      y = y0;
      z = z0;
      value = val;
      visited = false;
    }

    void setValue(int val)
    {
      value = val;
    }
};

class Map
{
  private:

    MapDimension mapdim;

    std::vector<std::vector<std::vector<Point>>> map;

    // Offset functions
    int xoffset(int xin) { return xin - x_min; }
    int yoffset(int yin) { return yin - y_min; }
    int zoffset(int zin) { return zin - z_min; }

  public:

    // Map Extents
    int x_min, x_max;
    int y_min, y_max;
    int z_min, z_max;

    Map() {}

    Map(int xmin, int xmax, int ymin, int ymax, int zmin=0, int zmax=0)
    {
      x_min = xmin;
      x_max = xmax;
      y_min = ymin;
      y_max = ymax;
      z_min = zmin;
      z_max = zmax;

      if (zmin == zmax)
      {
        mapdim = Map2D;
      }
      else
      {
        mapdim = Map3D;
      }

      int xsize = x_max-x_min+1;
      int ysize = y_max-y_min+1;
      int zsize = z_max-z_min+1;

      // Allocate map
      map.resize(xsize, std::vector<std::vector<Point>>(ysize, std::vector<Point>(zsize)));

      map[0][0][0] = Point(0,0);

      // Init points in map
      for (int i = x_min; i <= x_max; i++)
      {
         for (int j = y_min; j <= y_max; j++)
        {
          for (int k = z_min; k <= z_max; k++)
          {
            map[xoffset(i)][yoffset(j)][zoffset(k)] = Point(i, j, k);
          }
        }
      }
    }

    Point* point(int x, int y, int z=0)
    {
      if (pointExists(x, y, z))
      {
        return &map[xoffset(x)][yoffset(y)][zoffset(z)];
      }
      else
      {
        std::cout << 
          "ERROR: Tried to request invalid point "
          "(" << x << "," << y << "," << z << ")\n";
        exit(EXIT_FAILURE);
      }
      
    }

    std::vector<Point*> getAllAdjacent(Point * const &pt)
    {
      std::vector<Point*> neighbors;

      if (mapdim == Map2D)
      {
        // Get 2D neighbors
        if (pointExists(pt->x  ,pt->y-1,pt->z)) { neighbors.emplace_back(point(pt->x  ,pt->y-1,pt->z)); }
        if (pointExists(pt->x-1,pt->y  ,pt->z)) { neighbors.emplace_back(point(pt->x-1,pt->y  ,pt->z)); }
        if (pointExists(pt->x+1,pt->y  ,pt->z)) { neighbors.emplace_back(point(pt->x+1,pt->y  ,pt->z)); }
        if (pointExists(pt->x  ,pt->y+1,pt->z)) { neighbors.emplace_back(point(pt->x  ,pt->y+1,pt->z)); }

        // Diagonal neighbors
        if (pointExists(pt->x-1,pt->y-1,pt->z)) { neighbors.emplace_back(point(pt->x-1,pt->y-1,pt->z)); }
        if (pointExists(pt->x-1,pt->y+1,pt->z)) { neighbors.emplace_back(point(pt->x-1,pt->y+1,pt->z)); }
        if (pointExists(pt->x+1,pt->y-1,pt->z)) { neighbors.emplace_back(point(pt->x+1,pt->y-1,pt->z)); }
        if (pointExists(pt->x+1,pt->y+1,pt->z)) { neighbors.emplace_back(point(pt->x+1,pt->y+1,pt->z)); }

      }
      else
      {
        // Get 3D neighbors
        //TODO: IMPLEMNET 3D
        std::cout << "ERROR: getAllAdjacent() not implemented for 3D maps!\n";
        exit(EXIT_FAILURE);
      }
      return neighbors;
    }

    std::vector<Point*> getNSEWNeighbors(Point * const &pt)
    {
      std::vector<Point*> neighbors;

      if (mapdim == Map2D)
      {
        // Get 2D neighbors
        if (pointExists(pt->x  ,pt->y-1,pt->z)) { neighbors.emplace_back(point(pt->x  ,pt->y-1,pt->z)); }
        if (pointExists(pt->x-1,pt->y  ,pt->z)) { neighbors.emplace_back(point(pt->x-1,pt->y  ,pt->z)); }
        if (pointExists(pt->x+1,pt->y  ,pt->z)) { neighbors.emplace_back(point(pt->x+1,pt->y  ,pt->z)); }
        if (pointExists(pt->x  ,pt->y+1,pt->z)) { neighbors.emplace_back(point(pt->x  ,pt->y+1,pt->z)); }
      }
      else
      {
        std::cout << "ERROR: getNSEWNeighbors() not implemented for 3D maps!\n";
        exit(EXIT_FAILURE);
      }
      return neighbors;
    }

    bool pointExists(int x, int y, int z=0)
    {
      if (isBetween(x, x_min, x_max) && isBetween(y, y_min, y_max) && isBetween(z, z_min, z_max))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    bool isBetween(int val, int valmin, int valmax)
    {
      if ((val >= valmin) && (val <= valmax))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    int manhattan_distance(Point pt1, Point pt2)
    {
      return std::abs(pt1.x-pt2.x) + std::abs(pt1.y-pt2.y) + std::abs(pt1.z-pt2.z);
    }

    bool isEquivalent(Map &other)
    {
      // Check dimensions match
      if (this->x_min != other.x_min || 
          this->x_max != other.x_max || 
          this->y_min != other.y_min || 
          this->y_max != other.y_max || 
          this->z_min != other.z_min || 
          this->z_max != other.z_max) { return false; }

      // Check all values match
      bool match = true;
      for (auto z = this->z_min; z <= this->z_max; z++)
      {
        for (auto y = this->y_min; y <= this->y_max; y++)
        {
          for (auto x = this->x_min; x <= this->x_max; x++)
          {
            if (this->point(x,y,z)->value != other.point(x,y,z)->value) { match = false; break; }
          }
        }
      }
      return match;
    }

/*     bool operator==(const Map &other)
    {
      // Check dimensions match
      if (this->x_min != other.x_min || 
          this->x_max != other.x_max || 
          this->y_min != other.y_min || 
          this->y_max != other.y_max || 
          this->z_min != other.z_min || 
          this->z_max != other.z_max) { return false; }

      // Check all values match
      bool match = true;
      for (auto z = this->z_min; z <= this->z_max; z++)
      {
        for (auto y = this->y_min; y <= this->y_max; y++)
        {
          for (auto x = this->x_min; x <= this->x_max; x++)
          {
            if (this->point(x,y,z)->value != other.point(x,y,z)->value) { match = false; break; }
          }
        }
      }
      return match;
    } */

};