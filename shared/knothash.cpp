#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iomanip>

std::string knotHash(std::string input)
{

  std::vector<int> lengths;

  const int NUM_MARKS = 256;

  // Salt (problem-defined, not user input)
  int salt[] = { 17, 31, 73, 47, 23 };

  // Convert each character to ASCII
  for (auto i = 0; i < input.size(); i++)
  {
    lengths.push_back(int(input[i]));
  }

  // Add salt
  for (auto s : salt) { lengths.push_back(s); }

  // Main variables
  std::vector<int> marks; marks.reserve(NUM_MARKS);
  int current = 0;
  int skip = 0;
  int round = 0;
  const int NUM_ROUNDS = 64;

  // Initialize circle
  for (auto i = 0; i < NUM_MARKS; i++)
  {
    marks.push_back(i);
  }

  // Run
  for (round = 1; round <= NUM_ROUNDS; round++)
  {
    for (auto & length : lengths)
    {
      // Rotate marks so beginning of section to be reversed is
      // at the beginning of the vector
      std::rotate(marks.begin(), marks.begin() + current, marks.end());

      // Reverse beginning section of vector up to length
      std::reverse(&marks[0], &marks[length]);

      // Rotate marks back to original positions
      std::rotate(marks.rbegin(), marks.rbegin() + current, marks.rend());

      // Perform skip
      current += (length + skip);
      while (current >= NUM_MARKS)
      {
        current -= NUM_MARKS;
      }
      skip += 1;
    }
  }

  // Build dense hash
  const int blockSize = 16;
  int numBlocks = NUM_MARKS / blockSize;
  std::vector<int> dense;
  for (int i = 0; i < numBlocks; i++)
  {
    int offset = blockSize * i;
    int thisBlockXOR = 
      marks[offset+ 0] ^ marks[offset+ 1] ^ marks[offset+ 2] ^ marks[offset+ 3] ^
      marks[offset+ 4] ^ marks[offset+ 5] ^ marks[offset+ 6] ^ marks[offset+ 7] ^
      marks[offset+ 8] ^ marks[offset+ 9] ^ marks[offset+10] ^ marks[offset+11] ^ 
      marks[offset+12] ^ marks[offset+13] ^ marks[offset+14] ^ marks[offset+15];
    dense.push_back(thisBlockXOR);

  }

  // Generate hex
  std::stringstream stream;
  for (auto n : dense)
  {
    stream << std::setfill('0') << std::setw(2) << std::hex << n;
  }
    
  return stream.str();
}