#include <iostream>
#include <array>
#include <cmath>

enum RotationDirection
{
  Clockwise,
  Counterclockwise
};

std::array<int,2> rotation(RotationDirection dir, std::array<int,2> vec)
{
    if (dir == Clockwise) {
        return { vec[0]*0 - vec[1]*-1, vec[0]*-1 + vec[1]*0};
    }
    else if (dir == Counterclockwise)
    {
        return { vec[0]*0 - vec[1]*1, vec[0]*1 + vec[1]*0};
    }
    else
    {
        std::cout << "ERROR: Unknown rotation direction.\n";
        exit(EXIT_FAILURE);
    }
    
}

// https://en.wikipedia.org/wiki/Rotation_matrix
/*
       | cosT  -sinT | 
  R =  |             |
       | sinT   cosT |

       | cosT  -sinT |   | x |   | xcosT-ysinT |
  Rv = |             | . |   | = |             |
       | sinT   cosT |   | y |   | xsinT+ycosT |
*/

/*int main(int argc, char** argv)
{
  std::array<int,2> dir1 = {0, 1};

  std::array<int,2> dir2 = {1, 0};

  std::array<int,2> dir3 = {-1, 0};

  std::array<int,2> dir4 = {0, -1};

  std::cout << rotation(Clockwise, dir1)[0] <<        " " << rotation(Clockwise, dir1)[1] <<        " Expect: 1 0" << "\n";

  std::cout << rotation(Counterclockwise, dir1)[0] << " " << rotation(Counterclockwise, dir1)[1] << " Expect: -1 0" << "\n";

  std::cout << rotation(Clockwise, dir2)[0] <<        " " << rotation(Clockwise, dir2)[1] <<        " Expect: 0 -1" << "\n";

  std::cout << rotation(Counterclockwise, dir2)[0] << " " << rotation(Counterclockwise, dir2)[1] << " Expect: 0 1" << "\n";

  std::cout << rotation(Clockwise, dir3)[0] <<        " " << rotation(Clockwise, dir3)[1] <<        " Expect: 0 1" << "\n";

  std::cout << rotation(Counterclockwise, dir3)[0] << " " << rotation(Counterclockwise, dir3)[1] << " Expect: 0 -1" << "\n";

  std::cout << rotation(Clockwise, dir4)[0] <<        " " << rotation(Clockwise, dir4)[1] <<        " Expect: -1 0" << "\n";

  std::cout << rotation(Counterclockwise, dir4)[0] << " " << rotation(Counterclockwise, dir4)[1] << " Expect: 1 0" << "\n";
  return 0;
}*/