#include <iostream>
#include <cstdlib>
#include <vector>
//#include <array>
#include <algorithm>


class Vec3D
{
  public:
    int64_t x, y, z;

    Vec3D(int x0=0, int y0=0, int z0=0)
    {
      x = x0;
      y = y0;
      z = z0;
    }

    bool operator == (const Vec3D &Ref)
    {
      return (this->x == Ref.x && this->y == Ref.y && this->z == Ref.z);
    }

    Vec3D operator+(const Vec3D &Ref)
    {
      return Vec3D(this->x + Ref.x, this->y + Ref.y, this->z + Ref.z);
    }
    
    Vec3D operator=(const Vec3D &Ref)
    {
      this->x = Ref.x; this->y = Ref.y; this->z = Ref.z;
      return *this;
    }
};