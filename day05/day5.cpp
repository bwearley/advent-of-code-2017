#include <iostream>
#include <fstream>
#include <string>
#include <vector>

void Day5(std::string file)
{
  std::ifstream in(file);

  // Input
  std::vector<int> jumps_in;

  // Jumps
  std::vector<int> jumps;

  int numRows = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      jumps_in.push_back(std::stoi(line));   
    }
    numRows += 1;
  }

  // Part 1
  int numSteps = 0;
  int loc = 0;
  jumps = jumps_in;
  for (;;)
  {
    int current = jumps[loc];
    jumps[loc] += 1;
    loc += current;
    numSteps += 1;
    if (loc >= jumps.size()) { break; }
  }
  printf("Part 1: %d\n", numSteps); // 376976

  // Part 2
  numSteps = 0;
  loc = 0;
  jumps = jumps_in;
  for (;;)
  {
    int current = jumps[loc];
    if (current >= 3) {
      jumps[loc] -= 1;
    }
    else
    {
      jumps[loc] += 1;
    }    
    loc += current;
    numSteps += 1;
    if (loc >= jumps.size()) { break; }
  }
  printf("Part 2: %d\n", numSteps); // 29227751

}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day5(argv[1]);

  return 0;
}