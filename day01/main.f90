program main
    use syslog_mod
    use fclap_mod
    use file_tools_mod
    use string_tools_mod
    implicit none

    !-- Counters
    integer :: i, j

    !-- Input file unit
    integer :: input_unit

    !-- Number of digits in input file
    integer :: num_digits = 0
    
    integer :: total = 0
    
    integer,allocatable :: digits(:)

    ! Input file reading properties
    integer,parameter            :: max_line_len = 5000
    character(len=max_line_len)  :: line
    character(len=:),allocatable :: input_file

    !-- Initialize System Log
    call init_syslog

    !-- Process Command Line Arguments
    call configure_fclap
    call parse_command_line_arguments

    !-- Get input file name from command line
    input_file = get_value_for_arg('input_file')

    !-- Start timer
    call syslog % start_timer

    !-- Open file and read into memory
    open (                    &
        newunit = input_unit, &
        file    = input_file, &
        action  = 'read',     &
        status  = 'old',      &
        form    = 'formatted' &
    )
    read (input_unit,'(a)') line
    close(input_unit)

    !-- Allocate digits array
    num_digits = len(trim(line))
    allocate(digits(num_digits))

    do i = 1, num_digits
      read(line(i:i),*) digits(i)
    end do
    
    !-- Part 1
    total = 0
    do i = 1, num_digits - 1
      if (digits(i) == digits(i+1)) total = total + digits(i)
    end do
    if (digits(1) == digits(num_digits)) total = total + digits(1)
    write (          *,'(a,i0)'),'Part 1: ',total !1031
    write (syslog%unit,'(a,i0)'),'Part 1: ',total

    ! Part 2
    total = 0
    do i = 1, num_digits - 1
      j = i + (num_digits / 2)
      if (j > num_digits) j = (j - num_digits)
      if (digits(i) == digits(j)) total = total + digits(i)
    end do
    write (          *,'(a,i0)'),'Part 2: ',total !1080
    write (syslog%unit,'(a,i0)'),'Part 2: ',total
    
    !-- End timer
    call syslog % end_timer

    !-- Done
    call syslog%log(__FILE__,'Done.')

end program
