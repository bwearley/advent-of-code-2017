#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>

void Day1(std::string file)
{
  std::ifstream in(file);

  // Input
  std::vector<int> digits;

  // Answers
  int part1 = 0;
  int part2 = 0;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);

    if (!in.fail())
    {
      // Iterate through each character in string, convert to tmp char,
      // then convert to integer and store in digits
      for (auto it = line.cbegin(); it != line.cend(); ++it)
      {
        char tmp[2];
        tmp[1] = '\0';
        tmp[0] = *it;
        digits.push_back(std::atoi(tmp));
      }
    }
  }

  // Part 1
  for (auto i = 0; i < digits.size(); i++)
  {
    if (digits[i] == digits[i+1]) { part1 += digits[i]; }
  }
  if (digits[0] == digits[digits.size()-1]) { part1 += digits[0]; }
  printf("Part 1: %d\n", part1); // 1031

  // Part 2
  for (auto i = 0; i < digits.size(); i++)
  {
    int j = i + digits.size() / 2;
    if (j >= digits.size()) { j -= digits.size(); }
    if (digits[i] == digits[j]) { part2 += digits[i]; }
  }
  printf("Part 2: %d\n", part2); // 1080
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day1(argv[1]);

  return 0;
}