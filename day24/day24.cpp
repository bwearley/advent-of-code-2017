#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <array>
#include <algorithm>

#include "../shared/stringtools.cpp"

class Component
{
  public:
    std::string input;
    std::array<int,2> ports;
    int strength;
    
    Component(std::string in)
    {
      input = in;
      auto parts = split(in,'/');

      ports[0] = std::stoi(parts[0]);
      ports[1] = std::stoi(parts[1]);

      strength = ports[0] + ports[1];
    }

    bool fitsPort(int port)
    {
      return port == this->ports[0] || port == this->ports[1];
    }

    int other(int port)
    {
      if (port == this->ports[0]) return this->ports[1];
      if (port == this->ports[1]) return this->ports[0];
      std::cout << "ERROR: Unknown error with Component::other()." << std::endl;
      exit(EXIT_FAILURE);
    }
};

class Bridge
{
  public:
    std::vector<Component> bridgecmps;

    Bridge() {}

    void addComponent(Component cmp) { bridgecmps.push_back(cmp); }

    auto strength()
    {
      int strength = 0;
      for (auto & cmp : bridgecmps)
      {
        strength += cmp.strength;
      }
      return strength;
    }
};

std::vector<Bridge> allBridges;

auto compatibleComponents(std::vector<Component> &cmps, int port)
{
  std::vector<Component> compatible;
  std::copy_if(cmps.begin(),cmps.end(), std::back_inserter(compatible), [&](auto c) {return c.fitsPort(port);});
  return compatible;
}

void makeBridges(std::vector<Component> available, Bridge br_in = Bridge(), int port = 0)
{
  if (compatibleComponents(available, port).size() == 0 && br_in.bridgecmps.size() > 0)
  {
    allBridges.push_back(br_in);
    return;
  }

  for (auto i = 0; i < available.size(); i++)
  {
    if (available[i].fitsPort(port))
    {
      int newPort = available[i].other(port);
      
      auto availCpy = available;
      availCpy.erase(availCpy.begin()+i);

      auto br = br_in;

      br.addComponent(available[i]);
      makeBridges(availCpy, br, newPort);
    }
  }
}

void Day24(std::string file)
{
  std::ifstream in(file);

  std::vector<Component> components;

  // Load file data
  while (!in.eof())
  {
    std::string line;
    std::getline(in, line);
    if (!in.fail())
    {
      components.push_back(Component(line));
    }
  }

  makeBridges(components);
  //printf("Number of Bridges: %d\n", allBridges.size());

  // Part 1
  int maxStrength = 0;
  for (auto & br : allBridges)
  {
    maxStrength = std::max(maxStrength, br.strength());
  }
  printf("Part 1: %d\n", maxStrength); // 1940

  // Part 2
  int maxLength = 0;
  maxStrength = 0;
  for (auto & br : allBridges)
  {
    if (br.bridgecmps.size() >= maxLength)
    {
      maxLength = br.bridgecmps.size();
      maxStrength = std::max(maxStrength, br.strength());
    }
  }
  printf("Part 2: %d\n", maxStrength); // 1928
}

int main(int argc, char** argv)
{

  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day24(argv[1]);

  return 0;
}