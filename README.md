# Advent of Code 2017

My Advent of Code 2017 submissions in C++.

To build and run each solution,

`$ make.sh; ./dayXX input.txt;`